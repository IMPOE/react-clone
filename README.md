[# INSTAGRAM HEROKU PAGE](https://dry-ridge-04673.herokuapp.com/signin)

    Team members :


    Roman


    Max


    Adel 


___________________________________________________________________________________________________________________________________

    Assignment for student # 1 ( Roman)
Server part: fully created backend, work with requests, tokens, mail client, encryption of passwords

Client side: login page, registration, saved posts, password change, password recovery, user search, photo change, post creation, redux creation, modal windows, explore, deploying  on heroku
___________________________________________________________________________________________________________________________________

    Assignment for student # 2 (Max)

Components of the page:

Person's nickname + Subscribe button. If the user has already subscribed to this page, there should be an Unsubscribe button instead of the Subscribe button.
Person's posts in chronological order in the form of a grid. Each post is a photo. On hovering over a photo - it is darkened, the number of likes and comments with the corresponding icons is shown at the top.
By clicking on one of the photos, a modal window appears, which contains a photo and comments to the post. When you click on a shaded area outside the modal, it should close. Follow/Unfollow functional. Routing to user page.
___________________________________________________________________________________________________________________________________

  Assignment for student # 3 (Adel)

Posts feed (left side of the screenshot below). Each post contains - a photo, a like button, a field for adding a comment, and comments, if any. Only the last comment should be shown. If there are more than 1 comments, the Show more button should appear, by clicking on which, all comments are displayed. By double-clicking on the photo, the post is liked. When a post is liked, the like icon should turn a different color. The first three posts are loaded immediately, the rest of the posts must be loaded using the Infinity scroll technology. For implementation, you can use any library, or implement it yourself.
List of people the person is following (upper right part of the screenshot). Each "person" is an icon and a nickname. It should be a clickable link that should take you to that person's posts page when you click.
List of people to follow (bottom right of the screenshot). Each "person" is an icon, a nickname, and a Subscribe button. Icon and nickname - clickable link, when you click on which you should be taken to the page of this person's posts. By clicking on the Subscribe button, the system subscribes you to updates of this person. The button name changes to Unsubscribe. This list should not include people to whom the person has already subscribed. When you reload the page, the people you followed in the second list should be shown already in the first list.

___________________________________________________________________________________________________________________________________

    Used Technologies :

    server : 

    "bcrypt": "^5.0.1",
    "bcryptjs": "^2.4.3",
    "body-parser": "^1.19.0",
    "cookie-parser": "^1.4.5",
    "cors": "^2.8.5",
    "dotenv": "^10.0.0",
    "express": "^4.17.1",
    "express-validator": "^6.12.0",
    "i": "^0.3.6",
    "jsonwebtoken": "^8.5.1",
    "mongodb": "^3.6.8",
    "mongoose": "^5.12.14",
    "nodemailer": "^6.6.2",
    "npm": "^7.18.1",
    "request-ip": "^2.1.3",
    "session-mongoose": "^0.5.2",
    "uuid": "^8.3.2"

    "bson": "^1.1.6",
    "concurrently": "^6.2.0",
    "express-session": "^1.17.2",
    "nodemon": "^2.0.7"


    front : 

    "@material/image-list": "^11.0.0",
    "@testing-library/jest-dom": "^5.14.1",
    "@testing-library/react": "^11.2.7",
    "@testing-library/user-event": "^12.8.3",
    "axios": "^0.21.1",
    "material-ui-popup-state": "^1.8.4",
    "materialize-css": "^1.0.0",
    "node-sass": "^6.0.1",
    "react": "^17.0.2",
    "react-dom": "^17.0.2",
    "react-scripts": "4.0.3",
    "redux": "^4.1.0",
    "redux-thunk": "^2.3.0",
    "styled-components": "^5.3.0",
    "use-force-update": "^1.0.7",
    "web-vitals": "^1.1.2"

    "@material-ui/core": "^4.12.1",
    "@material-ui/icons": "^4.11.2",
    "concurrently": "^6.2.0",
    "formik": "^2.2.9",
    "history": "^5.0.0",
    "prop-types": "^15.7.2",
    "react-redux": "^7.2.4",
    "react-router-dom": "^5.2.0",
    "redux-devtools-extension": "^2.13.9",
    "sass": "^1.35.1",
    "yup": "^0.32.9"


___________________________________________________________________________________________________________________________________

