import React from 'react';
import { useHistory } from 'react-router-dom';
import ButtonMaterial from '@material-ui/core/Button/Button';

function Page404() {
  const history = useHistory();

  const goToHome = () => {
    history.push('/home');
  };

  return (
    <div style={{ paddingTop: 100, margin: '0 auto' }}>
      <div>
        <h1 style={{ textAlign: 'center' }}>Ooopppss...</h1>
        <h1 style={{ textAlign: 'center' }}>
          404 This page could not be found
        </h1>
      </div>
      <div style={{ textAlign: 'center', marginTop: 50 }}>
        <ButtonMaterial
          style={{ width: '25%' }}
          variant="contained"
          color={'primary'}
          onClick={goToHome}
        >
          Go to home page
        </ButtonMaterial>
      </div>
    </div>
  );
}

export default Page404;
