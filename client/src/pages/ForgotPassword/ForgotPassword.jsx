import React, { useState } from 'react';
import { Link, useHistory } from 'react-router-dom';
import LockIcon from '@material-ui/icons/Lock';
import { useDispatch } from 'react-redux';
import { getPassword } from '../../store/actions/userActions';
import ForgotPasswordForm from './ForgotPasswordForm/ForgotPasswordForm';
import './ForgotPassword.scss';
const ForgotPassword = () => {
  const dispatch = useDispatch();
  const history = useHistory();
  const [email, setEmail] = useState('');

  const PostData = async () => {
    await dispatch(getPassword(email.toLowerCase()));
    if (localStorage.getItem('changePassword')) {
      await history.push('/signin');
    }
    await localStorage.removeItem('changePassword');
  };
  return (
    <div className="forgot__password-container">
      <div className="login_auth">
        <div className="login__form">
          <div className="row">
            <div className="forgot__password-container-ico">
              <LockIcon className="forgot__password-container-lock" />
            </div>
            <p>Не удается войти?</p>
            <h6>
              Введите свой электронный адрес, и мы отправим вам ссылку для
              восстановления доступа к аккаунту.
            </h6>
            <ForgotPasswordForm
              email={email}
              setEmail={setEmail}
              postData={PostData}
            />
          </div>
        </div>
      </div>
      <div className="log_sign">
        <p>
          Есть аккаунт?
          <Link
            className="log_sign-link"
            to={{
              pathname: '/signin',
            }}
          >
            Вход
          </Link>
        </p>
      </div>
    </div>
  );
};

export default ForgotPassword;
