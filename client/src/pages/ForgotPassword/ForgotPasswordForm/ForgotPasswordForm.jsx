import React from 'react';
import { Link } from 'react-router-dom';
import './ForgotPasswordForm.scss';
const ForgotPasswordForm = ({ email, setEmail, postData }) => {
  return (
    <form className="col s12">
      <div className="row">
        <div className="input-field col s12">
          <input
            value={email}
            onChange={(e) => setEmail(e.target.value)}
            id="email"
            type="email"
            className="validate"
          ></input>
          <label htmlFor="email">Email</label>
        </div>
      </div>
      <div className="get__link">
        <a
          onClick={postData}
          className="waves-effect #64b5f6 blue lighten-2 btn-small"
        >
          get login link
        </a>
      </div>
      <div className="forgot__line">
        <h5 className="line"> Или </h5>
      </div>
      <div className="create__account">
        <Link to="/signup">Создать новый аккаунт</Link>
      </div>
    </form>
  );
};

export default ForgotPasswordForm;
