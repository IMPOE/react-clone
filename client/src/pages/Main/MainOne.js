import React, { useEffect } from 'react';
import Feed from './components/Feed/Feed';
import Sidebar from './components/Sidebar/Sidebar';
import { useDispatch, useSelector } from 'react-redux';
import './Main.scss';
import { getUserAction } from '../../store/actions/userActions';
import { getPosts, getSavedPostUsers } from '../../store/actions/postActions';

const MainOne = () => {
  const user = useSelector((state) => state.user);
  const dispatch = useDispatch();

  useEffect(async () => {
    await dispatch(getUserAction());
    await dispatch(getSavedPostUsers(user.id));
    await dispatch(getPosts());
  }, [dispatch]);

  return (
    <>
      <main className="main-page">
        <div className="container">
          <div className="main-page__inner">
            <div id="feed">
              <Feed />
            </div>
            <div id="sidebar">
              <Sidebar />
            </div>
          </div>
        </div>
      </main>
    </>
  );
};

export default MainOne;
