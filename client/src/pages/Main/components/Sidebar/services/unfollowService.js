import {
  getUser,
  getUserAction,
} from '../../../../../store/actions/userActions';
import { unfollowUserAction } from '../../../../../store/actions/subscribeAction';

export const unfollowUser = (userId, dispatch, following) => {
  fetch('/unfollow', {
    method: 'put',
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + localStorage.getItem('jwt'),
    },
    body: JSON.stringify({
      unfollowingId: userId,
    }),
  })
    .then((res) => res.json())
    .then(async (data) => {
      await localStorage.setItem('app_state', JSON.stringify(data));
      await dispatch(unfollowUserAction(data));
      await dispatch(getUserAction());
      await dispatch(getUser());
    });
};
