import { followUserAction } from '../../../../../store/actions/subscribeAction';
import {
  getUser,
  getUserAction,
} from '../../../../../store/actions/userActions';

export const followUser = (userId, dispatch, following) => {
  fetch('/follow', {
    method: 'put',
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + localStorage.getItem('jwt'),
    },
    body: JSON.stringify({
      followingId: userId,
    }),
  })
    .then((res) => res.json())
    .then(async (data) => {
      await localStorage.setItem('app_state', JSON.stringify(data));
      await dispatch(followUserAction(data));
      await dispatch(getUserAction());
      await dispatch(getUser());
    });
};
