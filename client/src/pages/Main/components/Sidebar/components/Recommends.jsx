import { UsersShortInfo } from './UsersShortInfo';

const Recommends = ({ title }) => {
  return (
    <div className="sidebar__recommends">
      <p className="sidebar__recommends__header">{title} :</p>
      <ul className="sidebar__recommends__container">
        <UsersShortInfo title={title} />
      </ul>
    </div>
  );
};

export default Recommends;
