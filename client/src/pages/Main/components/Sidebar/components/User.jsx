import React from 'react';
import { Link } from 'react-router-dom';
import { useSelector } from 'react-redux';
import { Avatar } from '@material-ui/core';

const User = () => {
  const userData = useSelector((state) => state.user);
  const { name, email, pic } = userData;

  return (
    <div className="sidebar__profile">
      <Link to={`/profile`}>
        <Avatar
          style={{ width: '5rem', height: '5rem' }}
          src={
            pic ||
            'https://www.pikpng.com/pngl/m/16-168770_user-iconset-no-profile-picture-icon-circle-clipart.png'
          }
          alt={name}
        />
      </Link>
      <div className="sidebar__profile__info">
        <Link to={`/profile`}>
          <p
            style={{ paddingLeft: '1rem' }}
            className="sidebar__profile__info--username"
          >
            {name}
          </p>
        </Link>
        <p
          style={{ paddingLeft: '1rem' }}
          className="sidebar__profile__info--name"
        >
          {email}
        </p>
      </div>
    </div>
  );
};

export default User;
