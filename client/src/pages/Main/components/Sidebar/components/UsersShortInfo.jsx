import { useDispatch, useSelector } from 'react-redux';
import { Avatar } from '@material-ui/core';
import Button from '../../../../../components/Button';
import { Link } from 'react-router-dom';
import { unfollowUser } from '../services/unfollowService';
import { followUser } from '../services/followService';
import { useEffect, useState } from 'react';
import '../../../../../App.css';
import axios from 'axios';
import { getUserAction } from '../../../../../store/actions/userActions';

export const UsersShortInfo = ({ title }) => {
  const dispatch = useDispatch();
  const following = useSelector((store) => store.user.following);
  const userOwnData = useSelector((state) => state.user) || [];
  const subcr = useSelector((state) => state.user.subscribes);

  const userSubscribes = subcr
    ? subcr.filter(
        ({ name, followers }) =>
          name !== userOwnData.name && followers.includes(userOwnData._id)
      )
    : [];
  const userRecommends = subcr
    ? subcr.filter(
        ({ name, followers }) =>
          name !== userOwnData.name &&
          followers.includes(userOwnData.id) === false
      )
    : [];

  return title === 'Your subscribes' ? (
    userSubscribes.length > 0 ? (
      userSubscribes.map((user, index) => {
        const { _id, pic, email, name } = user;

        return (
          <li className="sidebar__recommends__item" key={index}>
            <Link
              to={userOwnData?.name !== name ? '/profile/' + name : '/profile'}
            >
              <div className="sidebar__recommends__item__photo">
                <Avatar
                  src={
                    pic ||
                    'https://www.pikpng.com/pngl/m/16-168770_user-iconset-no-profile-picture-icon-circle-clipart.png'
                  }
                  alt={name}
                />
              </div>
            </Link>
            <div className="sidebar__recommends__item__info">
              <Link
                className="sidebar__recommends__item__info--username link"
                to={userOwnData?.name !== name ? '/profile/' + name : '/profile'}
              >
                {name}
              </Link>
              <p className="sidebar__recommends__item__info--name">{email}</p>
            </div>
            <Button
              btnClassName={
                userOwnData?.following.includes(_id) === true
                  ? 'sidebar__recommends__item__unsubscribe'
                  : 'sidebar__recommends__item__subscribe'
              }
              btnText={
                userOwnData?.following.includes(_id) === false
                  ? userOwnData?.followers.includes(_id)
                    ? 'Follow in reply'
                    : 'Follow'
                  : 'Unfollow'
              }
              onButtonClick={() => {
                userOwnData?.following.includes(_id) === true
                  ? unfollowUser(_id, dispatch, following)
                  : followUser(_id, dispatch, following);
              }}
            />
          </li>
        );
      })
    ) : (
      <h6>You have no subscribes yet!</h6>
    )
  ) : userRecommends?.length > 0 ? (
    userRecommends.map((user, index) => {
      const { _id, pic, email, name } = user;

      return (
        <li className="sidebar__recommends__item" key={index}>
          <Link
            to={userOwnData?.name !== name ? '/profile/' + name : '/profile'}
          >
            <div className="sidebar__recommends__item__photo">
              <Avatar
                src={
                  pic ||
                  'https://www.pikpng.com/pngl/m/16-168770_user-iconset-no-profile-picture-icon-circle-clipart.png'
                }
                alt={name}
              />
            </div>
          </Link>
          <div className="sidebar__recommends__item__info">
            <Link
              className="sidebar__recommends__item__info--username link"
              to={userOwnData?.name !== name ? '/profile/' + name : '/profile'}
            >
              {name}
            </Link>
            <p className="sidebar__recommends__item__info--name">{email}</p>
          </div>
          <Button
            btnClassName={
              userOwnData?.following.includes(_id) === true
                ? 'sidebar__recommends__item__unsubscribe'
                : 'sidebar__recommends__item__subscribe'
            }
            btnText={
              userOwnData?.following.includes(_id) === false
                ? userOwnData?.followers.includes(_id)
                  ? 'Follow in reply'
                  : 'Follow'
                : 'Unfollow'
            }
            onButtonClick={() => {
              userOwnData?.following.includes(_id) === true
                ? unfollowUser(_id, dispatch)
                : followUser(_id, dispatch, following);
            }}
          />
        </li>
      );
    })
  ) : (
    <h6>No recommends users for you :(</h6>
  );
};
