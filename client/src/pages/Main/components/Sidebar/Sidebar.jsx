import Recommends from './components/Recommends';
import User from './components/User';
import './Sidebar.scss';

const Sidebar = () => {
  return (
    <div className="sidebar">
      <User />
      <Recommends title="Recommends for you" />
      <Recommends title="Your subscribes" />
    </div>
  );
};

export default Sidebar;
