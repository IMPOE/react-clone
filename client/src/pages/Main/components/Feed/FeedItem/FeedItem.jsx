import React, { useEffect, useState } from 'react';
import FeedItemHeader from './components/FeedItemHeader';
import FeedItemStats from './components/FeedItemStats';
import FeedItemComments from './components/FeedItemComments';
import LikedIcon from '../../../../../components/assests/LikedIcon';
import PostModal from '../../../../../components/PostModal/PostModal';
import userSelector from '../../../../../store/selectors/userSelector';
import { useDispatch, useSelector } from 'react-redux';
import {
    dislikePostAction,
    getPosts,
    getSubPost,
    likePostAction,
    savePostAction,
} from '../../../../../store/actions/postActions';
import './FeedItem.scss';
import { getUsersPhoto } from '../../../../../store/actions/userActions';

const FeedItem = ({ post }) => {
    const dispatch = useDispatch();
    const [savePostAct, setSavePostAct] = useState(false);
    const [savedPostBookmark, setSavedPostBookmark] = useState(false);
    const user = useSelector(userSelector);
    const [timer, setTimer] = useState('');
    const picture = useSelector((store) => store.user.pic);
    const [openedPost, setOpenedPost] = useState(null);
    const { _id, photo, postedBy, likes, comments, createdAt, savedPost } = post;
    const date = new Date(createdAt);
    const nowDate = new Date();
    const userPostDate = nowDate - date;
    const [userLikedColor, setUserLikedColor] = useState(false);
    const following = useSelector((store) => store.user.following);
    const [deleted, setDeleted] = useState(false);

    useEffect(async () => {
        await dispatch(getUsersPhoto(postedBy?.id));
        await likes.filter(async (el) => {
            if (el?.id === user?.id) {
                await setUserLikedColor(true);
            } else {
                await setUserLikedColor(false);
            }
        });
        await savedPost.filter((el) => {
            if (el === user?.id) {
                setSavedPostBookmark(true);
            } else {
                setSavedPostBookmark(false);
            }
        });

        setTimer(msToTime(userPostDate));
    }, [likes, dispatch]);

    function msToTime(duration) {
        let minutes = parseInt((duration / (1000 * 60)) % 60),
            hours = parseInt((duration / (1000 * 60 * 60)) % 24),
            days = parseInt(duration / (1000 * 60 * 60 * 24));

        hours = hours < 10 ? '0' + hours : hours;
        minutes = minutes < 10 ? '0' + minutes : minutes;

        return `${
            days !== 0 ? `${days} дней` : ''
        } ${hours} час   ${minutes} минут`;
    }

    const isLiked =
        likes.filter((el) => {
            if (el?.id === user?.id) {
                return el;
            } else {
                return false;
            }
        }) || [];

    const likePostHandle = (post) => {
        if (isLiked?.length !== 0) {
            return (
                dispatch(dislikePostAction(_id, user?.id, user?.name, picture)),
                    setTimeout(async () => {
                        await dispatch(getPosts());
                        await dispatch(getSubPost(following));
                        setUserLikedColor(false);
                    }, 300)
            );
        } else {
            if (isLiked?.length === 0) {
                return (
                    dispatch(likePostAction(_id, user?.id, user?.name, picture)),
                        setTimeout(async () => {
                            await dispatch(getPosts());
                            await dispatch(getSubPost(following));
                            setUserLikedColor(true);
                        }, 300)
                );
            }
        }
    };

    const savePost = async (e) => {
        setSavedPostBookmark(true);
        setSavePostAct(true);
        await dispatch(savePostAction(e.target.id, user?.id));
        await dispatch(getPosts());
        await dispatch(getSubPost(following));
    };

    return (
        <div
            className="post-item"
            style={deleted ? { display: 'none' } : { display: 'inherit' }}
        >
            {openedPost && (
                <PostModal post={post} onClose={() => setOpenedPost(null)} />
            )}
            <FeedItemHeader
                postedBy={postedBy}
                idPost={_id}
                setDeleted={setDeleted}
            />
            <div className="post-item__body">
                <div
                    className="post-item__body__overlay"
                    onDoubleClick={() => likePostHandle(post?._id)}
                >
                    <LikedIcon />
                </div>
                <img src={photo} alt="post-content" id={post?._id} />
            </div>
            <div style={{ display: 'flex', flexDirection: 'column' }}>
                <p
                    style={{
                        fontSize: '1rem',
                        fontWeight: 'bold',
                        paddingLeft: '15px',
                        lineHeight: '5px',
                    }}
                >
                    {post.postedBy.name}:
                </p>
                <p
                    style={{ lineHeight: '5px', paddingLeft: '2rem', maxHeight: '2rem' }}
                >
                    {post.body}
                </p>
            </div>
            <p
                style={{
                    textAlign: 'right',
                    paddingRight: '2rem',
                    fontSize: '0.9em',
                    color: 'gray',
                }}
            >{`Добавлено ${timer} назад`}</p>
            <FeedItemStats
                isLiked={likes}
                userLikedColor={userLikedColor}
                likesCount={likes && likes.length}
                likePostHandle={() => likePostHandle()}
                setOpenedPost={() => setOpenedPost(post)}
                savePost={savePost}
                savePostAct={savePostAct}
                post={post}
                pic={post?.pic}
                id={_id}
                savedPostBookmark={savedPostBookmark}
            />
            <FeedItemComments postId={_id} comments={comments} postedBy={postedBy} />
        </div>
    );
};

export default FeedItem;
