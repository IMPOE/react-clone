import React, { useEffect, useState } from 'react';
import LikeIcon from '../../../../../../components/assests/LikeIcon';
import DirectIcon from '../../../../../../components/assests/DirectIcon';
import CommentIcon from '../../../../../../components/assests/CommentIcon';
import BookmarkIcon from '../../../../../../components/assests/BookmarkIcon';
import LikedIcon from '../../../../../../components/assests/LikedIcon';
import { Avatar } from '@material-ui/core';
import { useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
const FeedItemStats = ({
  isLiked,
  savedPostBookmark,
  likesCount,
  setOpenedPost,
  likePostHandle,
  userLikedColor,
  savePost,
  savePostAct,
  post = [],
  pic,
  id,
}) => {
  return (
    <div className="post-item__stats">
      <div className="post-item__stats__actions">
        <div className="post-item__stats__actions__inner">
          {isLiked ? (
            <LikedIcon
              style={{ disabled: 'true' }}
              color={userLikedColor ? 'red' : 'black'}
              className="post-item__stats__actions__icon"
              onClick={likePostHandle}
            />
          ) : (
            <LikeIcon
              className="post-item__stats__actions__icon"
              color={userLikedColor ? 'black' : 'red'}
              onClick={likePostHandle}
            />
          )}
          <CommentIcon
            className="post-item__stats__actions__icon"
            onClick={() => setOpenedPost()}
          />
          <DirectIcon className="post-item__stats__actions__icon" />
        </div>
        <BookmarkIcon
          id={id}
          className="post-item__stats__actions__icon"
          onClick={savePost}
          color={savedPostBookmark ? 'red' : 'black'}
        />
      </div>
      <div className="post-item__stats__likes" style={{ display: 'flex' }}>
        <div
          style={{ padding: '0 5px', display: 'flex', alignItems: 'center' }}
        >
          {likesCount > 0 ? (
            <Avatar src={pic} style={{ marginRight: '10px' }} />
          ) : (
            ''
          )}{' '}
          {likesCount} отметок "Нравится"
        </div>
      </div>
    </div>
  );
};

export default FeedItemStats;
