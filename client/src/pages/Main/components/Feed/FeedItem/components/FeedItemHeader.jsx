import React, { useEffect, useState } from 'react';
import { NavLink } from 'react-router-dom';
import MenuPopupState from './HeaderOption/HeaderOption';
import { useDispatch, useSelector } from 'react-redux';

const FeedItemHeader = ({ idPost, postedBy, setDeleted }) => {
  const { id, pic } = useSelector((state) => state.user);
  const user = [
    {
      id: id,
      pic: pic,
    },
  ];

  const photos = useSelector((state) => state.user.photos);
  const allData = [...user, ...photos];
  const [load, setLoad] = useState(false);

  useEffect(async () => {
    await setTimeout(() => {
      setLoad(true);
    }, 1000);
  }, []);

  const filtredPost = allData.filter((el) => {
    if (el && el?.id === postedBy?.id) {
      return el;
    }
  });


  const userOwnData = useSelector((state) => state.user);
  return (
    <div className="post-item__header" style={{ marginBottom: '1rem' }}>
      <NavLink
        className="post-item__header__link"
        to={
          userOwnData?.name !== postedBy?.name
            ? '/profile/' + postedBy?.name
            : '/profile'
        }
      >
        <div className="post-item__header__image">
          <img
            src={postedBy?.pic}
            alt={postedBy?.name}
          />
        </div>
        <div className="post-item__header__username">
          <p>{postedBy?.name}</p>
        </div>
      </NavLink>
      <div className="post-item__header__options">
        <MenuPopupState
          id={idPost}
          setDeleted={setDeleted}
          postedBy={postedBy}
        />
      </div>
    </div>
  );
};

export default FeedItemHeader;
