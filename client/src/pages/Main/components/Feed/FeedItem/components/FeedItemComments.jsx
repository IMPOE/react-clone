import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import userSelector from '../../../../../../store/selectors/userSelector';
import AddPostComment from '../../../../../../components/AddPostComment/AddPostComment';
import AllCommentsButton from './AllCommentsButton/AllCommentsButton';
import { Link } from 'react-router-dom';
import '../../../../../../App.css';

const FeedItemComments = ({ postId, comments = [], postedBy }) => {
  const { _id: authorizedUserId } = useSelector(userSelector);
  const [isAllComments, setIsAllComments] = useState(false);
  const userOwnData = useSelector((state) => state.user);

  const renderComments = (comments) =>
    comments.map(({ text = 'none', data }, index) => {
      return (
        <p key={index}>
          <Link
            className={'link'}
            to={
              userOwnData.name !== data?.name
                ? '/profile/' + data?.name
                : '/profile'
            }
          >
            <span className="last-comment-author">{data?.name}:</span>
          </Link>
          <span className="last-comment-text">{text}</span>
        </p>
      );
    });

  return (
    <div>
      <div>
        {comments.length > 0 && (
          <div className="last-comment">
            {renderComments(isAllComments ? comments : comments.slice(0, 1))}
          </div>
        )}
      </div>
      <div className="all-comments-button__wrapper">
        {comments.length > 1 && (
          <AllCommentsButton
            commentsCount={comments.length}
            showAllComments={setIsAllComments}
            isAllComments={isAllComments}
          />
        )}
      </div>
      <AddPostComment postId={postId} userId={authorizedUserId} />
    </div>
  );
};

export default FeedItemComments;
