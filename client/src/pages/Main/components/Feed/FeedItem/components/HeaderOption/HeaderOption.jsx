import React, { useState } from 'react';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import PopupState, { bindTrigger, bindMenu } from 'material-ui-popup-state';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import {
  deletePost,
  getSubPost,
} from '../../../../../../../store/actions/postActions';
import { useDispatch, useSelector } from 'react-redux';

export default function MenuPopupState({ id, setDeleted, postedBy }) {
  const dispatch = useDispatch();
  const userId = useSelector((state) => state.user.id);
  const following = useSelector((store) => store.user.following);
  const [close, setClose] = useState(false);
  function deleteUserPost(e) {
    const postId = e.target.id;
    if (userId === postedBy.id) {
      setClose(true);
      dispatch(deletePost(postId));
      dispatch(getSubPost(following));
      setDeleted(true);
    } else {
      dispatch(deletePost(postId));
    }
  }

  return (
    <PopupState variant="popover" popupId="demo-popup-menu">
      {(popupState) => (
        <React.Fragment>
          <MoreVertIcon
            variant="contained"
            color="primary"
            {...bindTrigger(popupState)}
          ></MoreVertIcon>
          <Menu
            {...bindMenu(popupState)}
            style={close ? { display: 'none' } : { display: 'inherit' }}
          >
            <MenuItem
              onClick={(e) => {
                deleteUserPost(e);
              }}
              id={id}
            >
              Delete post
            </MenuItem>
            <MenuItem onClick={popupState?.close}>Copy Url</MenuItem>
          </Menu>
        </React.Fragment>
      )}
    </PopupState>
  );
}
