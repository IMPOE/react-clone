import React, { useEffect } from 'react';
import FeedItem from './FeedItem/FeedItem';
import { useDispatch, useSelector } from 'react-redux';
import './Feed.scss';
import { getSubPost } from '../../../../store/actions/postActions';

const Feed = () => {
  const dispatch = useDispatch();
  const userPosts = useSelector((state) => state.post.data.post) || [];
  const following = useSelector((store) => store.user.following) || [];
  const feed = useSelector((state) => state.post.subPost.posts) || [];
  const userFeedPosts = [...userPosts, ...feed];

  useEffect(async () => {
    await dispatch(getSubPost(following));
  }, []);

  const feedItemComponents =
    userFeedPosts.map((post) => <FeedItem post={post} key={post?._id} />) || [];

  return (
    <div className="post-container">
      {feedItemComponents?.length > 0 ? (
        feedItemComponents
      ) : (
        <p className="post-container__message">We dont find anything to show</p>
      )}
    </div>
  );
};

export default Feed;
