import * as React from 'react';
import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { getAllPosts, onePost } from '../../store/actions/postActions';
import './index.scss';
import UserProfileHeader from '../ProfilePage/UserProfileHeader/UserProfileHeader';
import { Link } from 'react-router-dom';
import UserPost from '../ProfilePage/UserPost/UserPost';
import Modal from '../../components/ModalWindow';
import ModalHeader from '../../components/ModalWindow/components/ModalHeader/ModalHeader';
import ModalFooter from '../../components/ModalWindow/components/ModalFooter/ModalFooter';
import {
  modalHideAction,
  modalShowAction,
} from '../../store/actions/modalAction';

export default function StandardImageList() {
  const dispatch = useDispatch();
  const post = useSelector((store) => store.post.allPost) || [];
  const { name, pic, id } = useSelector((state) => state.user);
  const modal = useSelector((state) => state.modal.isOpen);
  const posts = useSelector((state) => state.post.data.post);
  const [postId, setPostId] = useState(0 || []);
  const [postUrl, setPostUrl] = useState('');
  const modalPost = useSelector((store) => store.post.modalPost.userPost) || [];
  const [postFormodal, setpostFormodal] = useState({
    postedBy: {
      pic: 'aa',
      name: 'Default',
    },
  });
  const [userLikedColor, setUserLikedColor] = useState(false);
  useEffect(async () => {
    await dispatch(getAllPosts());
  }, [dispatch]);

  const showModal = (post) => {
    setpostFormodal(post);
    dispatch(modalShowAction());
    dispatch(onePost(post?._id));
  };
  const isLiked = modalPost.likes.filter((el) => {
    if (el?.id === id) {
      return el;
    } else {
      return false;
    }
  });

  const hideModal = () => {
    dispatch(modalHideAction());
  };

  return (
    <div className="user-profile-container">
      <UserProfileHeader />
      <div className={posts?.length > 0 ? 'user-profile__posts' : ''}>
        {post.length === 0 ? (
          <h4 className="no-photo">
            You haven`t any posts yet.
            <Link className="create-post" to={{ pathname: '/createpost' }}>
              {' '}
              Click here to post something
            </Link>
          </h4>
        ) : (
          <UserPost
            setpostFormodal={setpostFormodal}
            setPostId={setPostId}
            showModal={showModal}
            setPostUrl={setPostUrl}
            posts={post}
          />
        )}
        <Modal
          postUrl={postUrl}
          active={modal}
          id={`post${postId}`}
          onModalClose={hideModal}
          header={
            <ModalHeader
              pic={postFormodal?.postedBy.pic}
              name={postFormodal?.postedBy.name}
              postFormodal={postFormodal}
            />
          }
          subText={
            <h5 style={{ textAlign: 'center', margin: '10px 0 0 10rem' }}>
              No comments yet
            </h5>
          }
          comments={modalPost ? modalPost?.comments : []}
          postedBy={modalPost ? modalPost?.postedBy : []}
          footer={
            <ModalFooter
              isLiked={isLiked}
              postFormodal={postFormodal}
              modalPost={modalPost}
              postId={postId}
              pic={pic}
              name={name}
              showModal={showModal}
              setUserLikedColor={setUserLikedColor}
              id={id}
            />
          }
        />
      </div>
    </div>
  );
}
