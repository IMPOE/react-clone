import React, { useEffect, useState } from 'react';
import { Avatar, Tooltip } from '@material-ui/core';
import ButtonMaterial from '@material-ui/core/Button/Button';
import { Link } from 'react-router-dom';
import Button from '../../../../components/Button';
import TransitionsModal from '../../../../components/AddPhotoModal/AddPhotoModal';
import InformativeModal from '../../../../components/InformativeModal';
import {
  getUserActionForModal,
  getUserInfo,
} from '../../../../store/actions/userActions';
import { useDispatch } from 'react-redux';
import './userHeaderProf.scss';
const UserProfileHeader = ({
  username,
  ProfileData,
  userProfileData,
  userPostsData,
  isSubscribed,
  userData,
  followUser,
  unfollowUser,
  setProfileData,
  chengeParam,
  setPostsData,
}) => {
  const dispatch = useDispatch();
  const [modal, setModal] = useState(false);
  const [target, setTarget] = useState(null);
  const [userFollowers, setUserFollowers] = useState([]);
  const [userFollowing, setUserFollowing] = useState([]);
  const [classModal, setClassModal] = useState([]);
  const showInfoModal = async (e) => {
    if (e.target.id === 'following') {
      await setClassModal(userFollowers);
    }
    if (e.target.id === 'followers') {
      await setClassModal(userFollowing);
    }
    setModal(true);
  };

  const hideInfoModal = () => {
    setModal(false);
  };

  useEffect(async () => {
    await dispatch(getUserInfo(username, setProfileData, setPostsData));
    await dispatch(
      getUserActionForModal(setUserFollowers, setUserFollowing, userProfileData)
    );
  }, []);

  const getUsersData = async () => {
    await dispatch(
      getUserActionForModal(setUserFollowers, setUserFollowing, userProfileData)
    );
  };

  return (
    <div className="user-profile-header">
      <div className="user-profile__logo">
        <Avatar
          src={
            ProfileData
              ? userProfileData?.pic
              : 'https://www.pikpng.com/pngl/m/16-168770_user-iconset-no-profile-picture-icon-circle-clipart.png'
          }
          alt="logo"
          style={{ width: '12rem', height: '12rem' }}
        />
      </div>
      <div className="user__profile-container">
        <h5>{ProfileData ? userProfileData?.name : ''}</h5>
        <div className="user-profile__description">
          <p className={'user-data'}>{userPostsData?.length} posts</p>
          <p
            id="followers"
            className={'user-data'}
            onClick={async (e) => {
              setTarget(e.target.innerText);
              await getUsersData();
              await showInfoModal(e);
            }}
          >
            {ProfileData
              ? ProfileData?.followers.length
              : userProfileData?.followers.length}{' '}
            followers
          </p>
          <p
            id="following"
            className={'user-data'}
            onClick={async (e) => {
              setTarget(e.target.innerText);
              await getUsersData();
              await showInfoModal(e);
            }}
          >
            {userProfileData?.following.length} followings
          </p>
        </div>

        <div
          className="addPost"
          style={{
            width: '100%',
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            padding: '20px 0',
          }}
        >
          {userProfileData.name !== userData?.name ? (
            <div>
              <ButtonMaterial
                id="user__btn_sub"
                variant="contained"
                color={isSubscribed === false ? 'primary' : 'secondary'}
                onClick={() => {
                  isSubscribed === false ? followUser() : unfollowUser();
                }}
              >
                {isSubscribed === false
                  ? userData?.followers.includes(userProfileData?._id)
                    ? `Follow in reply ${userProfileData?.name}`
                    : `Follow ${userProfileData?.name}`
                  : `Unfollow ${userProfileData?.name}`}
              </ButtonMaterial>
            </div>
          ) : (
            <>
              <div>
                <Tooltip title="Create Post" arrow>
                  <Link
                    to={{
                      pathname: '/createpost',
                    }}
                  >
                    <Button
                      btnClassName={'btn btn-primary btn-Small'}
                      style={{ marginRight: 10 }}
                      btnText="Create Post"
                    />
                  </Link>
                </Tooltip>
              </div>
              <div>
                <Tooltip title="Upload Logo" arrow>
                  <TransitionsModal />
                </Tooltip>
              </div>
            </>
          )}
          <InformativeModal
            setPostsData={setPostsData}
            chengeParam={chengeParam}
            username={username}
            setProfileData={setProfileData}
            userName={userProfileData?.name}
            hideModal={hideInfoModal}
            validationData={classModal ? 'followings' : 'followers'}
            active={modal}
            renderArr={classModal}
          />
        </div>
      </div>
    </div>
  );
};

export default UserProfileHeader;
