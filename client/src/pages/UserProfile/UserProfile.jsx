import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link, useParams } from 'react-router-dom';
import { onePost } from '../../store/actions/postActions';
import {
  modalHideAction,
  modalShowAction,
} from '../../store/actions/modalAction';
import Modal from '../../components/ModalWindow';
import './UserProfile.scss';
import Loading from '../../components/Loader';
import { getUser, getUserInfo } from '../../store/actions/userActions';
import {
  followUserAction,
  unfollowUserAction,
} from '../../store/actions/subscribeAction';
import UserProfileHeader from './components/UserProfileHeader/UserProfileHeader';
import ModalHeader from '../../components/ModalWindow/components/ModalHeader/ModalHeader';
import UserPost from '../ProfilePage/UserPost/UserPost';
import ModalFooter from '../../components/ModalWindow/components/ModalFooter/ModalFooter';

const UserProfile = () => {
  const dispatch = useDispatch();
  const { id } = useSelector((state) => state.user);
  const [userProfileData, setProfileData] = useState(null);
  const [userPostsData, setPostsData] = useState([]) || [];
  const [postId, setPostId] = useState('');
  const [postUrl, setPostUrl] = useState('');
  const modalPost =
    useSelector((store) => store.post.modalPost.userPost) || null;
  const userData = useSelector((state) => state.user);
  const modal = useSelector((state) => state.modal.isOpen);
  const [postFormodal, setpostFormodal] = useState([]);
  const [isSubscribed, setSubscribed] = useState(false);
  const { username } = useParams();
  const { name, pic } = userData;
  const [userLikedColor, setUserLikedColor] = useState(false);

  async function chengeParam() {
    setTimeout(async () => {
      await dispatch(getUserInfo(username, setProfileData, setPostsData));
    }, 0);
  }

  useEffect(async () => {
    await dispatch(getUserInfo(username, setProfileData, setPostsData));
  }, [modal]);

  useEffect(() => {
    if (userProfileData) {
      const result = userProfileData.followers.includes(userData?._id);
      result === false ? setSubscribed(false) : setSubscribed(true);
    }
  }, [userProfileData, setProfileData]);

  const hideModal = () => {
    dispatch(modalHideAction());
  };
  const showModal = (post) => {
    setpostFormodal(post);
    dispatch(onePost(post?._id));
    dispatch(modalShowAction());
  };

  const isLiked = modalPost.likes.filter((el) => {
    if (el.id === id) {
      return el;
    } else {
      return false;
    }
  });

  const followUser = () => {
    fetch('/follow', {
      method: 'put',
      headers: {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + localStorage.getItem('jwt'),
      },
      body: JSON.stringify({
        followingId: userProfileData._id,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        localStorage.setItem('app_state', JSON.stringify(data));
        dispatch(followUserAction(data));
        dispatch(getUser());
        setProfileData((prevState) => {
          return {
            ...prevState,
            followers: [...prevState.followers, data?._id],
          };
        });
      });
  };

  const unfollowUser = () => {
    fetch('/unfollow', {
      method: 'put',
      headers: {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + localStorage.getItem('jwt'),
      },
      body: JSON.stringify({
        unfollowingId: userProfileData?._id,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        localStorage.setItem('app_state', JSON.stringify(data));
        dispatch(unfollowUserAction(data));
        dispatch(getUser());
        setProfileData((prevState) => {
          const unfollowedUser = prevState.followers.filter(
            (item) => item !== data?._id
          );
          return {
            ...prevState,
            followers: unfollowedUser,
          };
        });
      });
  };

  return (
    <div className="back">
      {userProfileData && userPostsData ? (
        <div className="user-profile-container">
          <UserProfileHeader
            chengeParam={chengeParam}
            username={username}
            setProfileData={setProfileData}
            userProfileData={userProfileData}
            userPostsData={userPostsData}
            isSubscribed={isSubscribed}
            userData={userData}
            followUser={followUser}
            unfollowUser={unfollowUser}
            setPostsData={setPostsData}
          />
          <div
            className={userPostsData?.length > 0 ? 'user-profile__posts' : ''}
          >
            {userPostsData?.length === 0 ? (
              <h4 className="no-photo">
                {userProfileData?.name !== userData?.name
                  ? `User ${userProfileData.name} havent posts yet`
                  : `You have not any post yet, 
                                    ${(
                                      <Link
                                        className="create-post"
                                        to={{ pathname: '/createpost' }}
                                      >
                                        {' '}
                                        Click here to post something
                                      </Link>
                                    )}`}
              </h4>
            ) : (
              <div className="greed">
                <UserPost
                  setPostId={setPostId}
                  showModal={showModal}
                  setPostUrl={setPostUrl}
                  posts={userPostsData}
                />
              </div>
            )}
            <Modal
              urlUserName={username}
              profileData={setProfileData}
              postsData={setPostsData}
              postUrl={postUrl}
              active={modal}
              id={`post${postId}`}
              onModalClose={hideModal}
              header={
                <ModalHeader
                  pic={userProfileData?.pic}
                  name={userProfileData?.name}
                />
              }
              subText={
                <h5 style={{ textAlign: 'center', margin: '10px 0 0 10rem' }}>
                  No comments yet
                </h5>
              }
              comments={modalPost ? modalPost?.comments : []}
              postedBy={modalPost ? modalPost?.postedBy : []}
              footer={
                <ModalFooter
                  isLiked={isLiked}
                  postFormodal={postFormodal}
                  modalPost={modalPost}
                  postId={postId}
                  pic={pic}
                  name={name}
                  showModal={showModal}
                  setUserLikedColor={setUserLikedColor}
                  id={id}
                />
              }
            />
          </div>
        </div>
      ) : (
        <div style={{ paddingTop: 300 }}>{<Loading />}</div>
      )}
    </div>
  );
};

export default UserProfile;
