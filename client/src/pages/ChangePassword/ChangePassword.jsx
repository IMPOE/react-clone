import React, { useState } from 'react';
import { Button, TextField } from '@material-ui/core';
import { newPassword } from '../../store/actions/userActions';
import { useDispatch, useSelector } from 'react-redux';
import './ChangePassword.scss';
import { useHistory } from 'react-router-dom';

const ChangePassword = () => {
  const dispatch = useDispatch();
  const history = useHistory();
  const [repeatPassword, setRepeatPassword] = useState('');
  const [password, setNewPassword] = useState('');
  const resetToken = useSelector((store) => store.user.resetToken);
  const PostData = async () => {
    await dispatch(newPassword(password, resetToken));
    if (localStorage.getItem('newPassword')) {
      await history.push('/home');
    }
    await localStorage.removeItem('newPassword');
  };
  return (
    <div
      style={{
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'column',
        width: '100%',
        height: '100vh',
      }}
    >
      <div style={{ padding: '25px' }}>
        <TextField
          value={repeatPassword}
          onChange={(e) => setRepeatPassword(e.target.value)}
          id="outlined-password-input-one"
          label="new password"
          type="password"
          variant="outlined"
        />
      </div>
      <div style={{ padding: '25px' }}>
        <TextField
          value={password}
          onChange={(e) => setNewPassword(e.target.value)}
          id="outlined-password-input-two"
          label=" repeat new password"
          type="password"
          variant="outlined"
        />
      </div>
      <div>
        <Button onClick={PostData} variant="outlined" color="primary">
          Primary
        </Button>
      </div>
    </div>
  );
};

export default ChangePassword;
