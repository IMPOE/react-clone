import './Auth.scss';
import React from 'react';
import SignIn from '../LoginPage/SignIn';
export default function Auth() {
  return (
    <div className="auth__container">
      <div className="auth__container-flex">
        {<img src="logoInst.png" alt="img" /> || (
          <img src="logo.png" alt="logo" />
        )}
      </div>
      <div className="auth__container-signin">
        <SignIn />
      </div>
    </div>
  );
}
