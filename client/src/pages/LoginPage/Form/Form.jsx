import React from 'react';
import { Link } from 'react-router-dom';

const Form = ({ email, setEmail, password, setPassword, PostData }) => {
  return (
    <form className="col s12">
      <div className="row">
        <div className="input-field col s12">
          <input
            value={email}
            onChange={(e) => setEmail(e.target.value)}
            id="email"
            type="email"
            className="validate"
          ></input>
          <label htmlFor="email">Имейл</label>
        </div>
      </div>
      <div className="row">
        <div className="input-field col s12">
          <input
            value={password}
            onChange={(e) => setPassword(e.target.value)}
            id="password"
            type="password"
            className="validate"
          ></input>
          <label htmlFor="password">Пароль</label>
        </div>
      </div>
      <div className="signin__container">
        <a
          onClick={PostData}
          className="waves-effect #64b5f6 blue lighten-2 btn-small"
        >
          Войти
        </a>
      </div>
      <div className="or__line">
        <h6 className="line"> Или </h6>
      </div>
      <p className="forgot__password-text">
        <Link
          to={{
            pathname: '/forgotpassword',
          }}
        >
          Забыли пароль?
        </Link>
      </p>
    </form>
  );
};

export default Form;
