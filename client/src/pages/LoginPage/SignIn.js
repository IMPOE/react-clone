import React, { useEffect, useState } from 'react';
import { Link, useHistory } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { clearUser, login } from '../../store/actions/userActions';
import './Login.scss';
import Form from './Form/Form';
const SignIn = () => {
  const dispatch = useDispatch();
  const history = useHistory();
  const [password, setPassword] = useState('');
  const [email, setEmail] = useState('');

  useEffect(async () => {
    await dispatch(clearUser());
  }, []);

  const PostData = async () => {
    await dispatch(login(password, email));
    await history.push('/home');
  };

  return (
    <div className="login_auth_container">
      <div className="login_auth">
        <div className="login__form">
          <div className="row">
            <div className="login__row">
              {<img src="https://svgsilh.com/svg/1594387.svg" alt="logo" /> || (
                <img src="logo.png" alt="logo" />
              )}
            </div>
            <Form
              email={email}
              setEmail={setEmail}
              password={password}
              setPassword={setPassword}
              PostData={PostData}
            />
          </div>
        </div>
      </div>
      <div className="log_sign">
        <p>
          Еще нет аккаунта?
          <Link
            className="link__signup"
            to={{
              pathname: '/signup',
            }}
          >
            Зарегистрируйтесь
          </Link>
        </p>
      </div>
    </div>
  );
};

export default SignIn;
