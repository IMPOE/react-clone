import React from 'react';
import './userPost.scss';
import LikedIcon from '../../../components/assests/LikedIcon';
import CommentIcon from '../../../components/assests/CommentIcon';
const UserPost = ({ setPostId, showModal, setPostUrl, posts }) => {
  const userPost = posts.map((post, index) => (
    <div className="post__container" key={post?._id}>
      <div className="post__container-head">
        <div
          className="post__container-body"
          data-modal={`post${post?._id}`}
          onClick={() => {
            setPostId(post?._id);
            showModal(post);
            setPostUrl(post?.photo);
          }}
        >
          <div
            className="img_item_description"
            style={{
              display: 'flex',
              justifyContent: 'center',
              alignItems: 'center',
            }}
          >
            <div style={{ padding: '0 6px', display: 'flex' }}>
              {<LikedIcon color="white" />}
              <div
                style={{ color: 'white', fontWeight: 'bold', padding: '0 3px' }}
              >
                {post?.likes.length}
              </div>
            </div>
            <div style={{ padding: '0 6px', display: 'flex' }}>
              <CommentIcon color="white" />
              <div
                style={{ color: 'white', fontWeight: 'bold', padding: '0 3px' }}
              >
                {post?.comments.length}
              </div>
            </div>
          </div>
        </div>
        <img
          className={posts.length > 0 ? 'posts__image' : ''}
          src={post?.photo}
          alt={`post${index}`}
          id={post?._id}
        />
      </div>
    </div>
  ));

  return userPost;
};

export default UserPost;
