import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import { getPosts, onePost } from '../../store/actions/postActions';
import {
  modalShowAction,
  modalHideAction,
} from '../../store/actions/modalAction';
import Modal from '../../components/ModalWindow';
import './Profile.scss';
import UserProfileHeader from './UserProfileHeader/UserProfileHeader';
import UserPost from './UserPost/UserPost';
import ModalHeader from '../../components/ModalWindow/components/ModalHeader/ModalHeader';
import ModalFooter from '../../components/ModalWindow/components/ModalFooter/ModalFooter';

export const Profile = (props) => {
  const dispatch = useDispatch();
  const { name, pic, id } = useSelector((state) => state.user);
  const modal = useSelector((state) => state.modal.isOpen);
  const posts = useSelector((state) => state.post.data.post);
  const [postId, setPostId] = useState(0 || []);
  const [postUrl, setPostUrl] = useState('');
  const modalPost = useSelector((store) => store.post.modalPost.userPost) || [];
  const [postFormodal, setpostFormodal] = useState([]) || [];
  const [userLikedColor, setUserLikedColor] = useState(false);
  const showModal = (post) => {
    setpostFormodal(post);
    dispatch(modalShowAction());
    dispatch(onePost(post._id));
  };

  useEffect(() => {
    dispatch(getPosts());
  }, []);

  const isLiked = modalPost.likes.filter((el) => {
    if (el?.id === id) {
      return el;
    } else {
      return false;
    }
  });

  const hideModal = () => {
    dispatch(modalHideAction());
  };

  return (
    <div className="back">
      <div className="user-profile-container">
        <UserProfileHeader />
        <div className={posts?.length > 0 ? 'user-profile__posts' : ''}>
          {posts?.length === 0 ? (
            <h4 className="no-photo">
              You haven`t any posts yet.
              <Link className="create-post" to={{ pathname: '/createpost' }}>
                {' '}
                Click here to post something
              </Link>
            </h4>
          ) : (
            <div className="greed">
              <UserPost
                setPostId={setPostId}
                showModal={showModal}
                setPostUrl={setPostUrl}
                posts={posts}
              />
            </div>
          )}
        </div>
      </div>
      <Modal
        postUrl={postUrl}
        active={modal}
        id={`post${postId}`}
        onModalClose={hideModal}
        header={<ModalHeader pic={pic} name={name} />}
        subText={
          <h5 style={{ textAlign: 'center', margin: '10px 0 0 10rem' }}>
            No comments yet
          </h5>
        }
        comments={modalPost ? modalPost?.comments : []}
        postedBy={modalPost ? modalPost?.postedBy : []}
        footer={
          <ModalFooter
            isLiked={isLiked}
            postFormodal={postFormodal}
            modalPost={modalPost}
            postId={postId}
            pic={pic}
            name={name}
            showModal={showModal}
            setUserLikedColor={setUserLikedColor}
            id={id}
          />
        }
      />
    </div>
  );
};
