import React, { useEffect } from 'react';
import {Tooltip } from '@material-ui/core';
import { Link } from 'react-router-dom';
import Button from '../../../components/Button';
import TransitionsModal from '../../../components/AddPhotoModal/AddPhotoModal';
import { useDispatch, useSelector } from 'react-redux';
import '../Profile.scss';
import { useState } from 'react';
import '../../../components/PostModal/components/PostModalComments/PostModalComments.scss';
import InformativeModal from '../../../components/InformativeModal';
import { getUserActionForModal } from '../../../store/actions/userActions';

const UserProfileHeader = () => {
  const dispatch = useDispatch();
  const userData = useSelector((state) => state.user);
  const { name, followers, following, pic } = userData;
  const [modal, setModal] = useState(false);
  const [userFollowers, setUserFollowers] = useState([]);
  const [userFollowing, setUserFollowing] = useState([]);
  const [target, setTarget] = useState(null);
  const [classModal, setClassModal] = useState([]);

  const showModal = async (e) => {
    setModal(false);
    if (e?.target.id === 'following') {
      await setClassModal(userFollowers);
    }
    if (e?.target.id === 'followers') {
      await setClassModal(userFollowing);
    }
    setModal(true);
  };
  const hideModal = () => {
    setModal(false);
  };

  useEffect(async () => {
    await dispatch(
      getUserActionForModal(setUserFollowers, setUserFollowing, userData)
    );
  }, []);

  const getUsersData = async () => {
    await dispatch(
      getUserActionForModal(setUserFollowers, setUserFollowing, userData)
    );
  };

  const posts = useSelector((state) => state?.post.data.post);
  return (
    <>
      <div className="user-profile-header">
        <div className="user-profile__logo">
          <Tooltip title="Upload Logo" arrow>
            <TransitionsModal pic={pic} />
          </Tooltip>
        </div>
        <div>
          <h5>{name}</h5>
          <Tooltip title="Edit profile" arrow>
            <Link to={{ pathname: '/editgprofile' }}>
              <Button
                style={{ margin: '0 10px 10px 0 ' }}
                btnClassName={'edit-btn'}
                btnText={'Edit profile'}
              />
            </Link>
          </Tooltip>
          <Tooltip title="Create Post" arrow>
            <Link
              to={{
                pathname: '/createpost',
              }}
            >
              <Button
                btnClassName={'btn btn-primary btn-Small'}
                id="create__post-btn"
                style={{
                  marginRight: 10,
                  height: '2.1rem',
                  position: 'relative',
                  bottom: '2px',
                }}
                btnText="Create Post"
              />
            </Link>
          </Tooltip>
          <div className="user-profile__description">
            <div>
              <Tooltip title="see posts" arrow>
                <p className={'user-data'}>
                  <span>{posts?.length}</span> posts
                </p>
              </Tooltip>
            </div>
            <div>
              <Tooltip title="see followers" arrow>
                <p
                  id="followers"
                  className={'user-data'}
                  onClick={async (e) => {
                    setTarget(e.target.innerText);
                    await getUsersData();
                    await showModal(e);
                  }}
                >
                  <span>{followers.length}</span> followers
                </p>
              </Tooltip>
            </div>
            <div>
              <Tooltip title="see followings" arrow>
                <p
                  id="following"
                  className={'user-data following '}
                  onClick={async (e) => {
                    setTarget(e.target.innerText);
                    await getUsersData();
                    await showModal(e);
                  }}
                >
                  <span>{following?.length}</span> followings
                </p>
              </Tooltip>
            </div>
          </div>

          <div
            className="addPost"
            style={{
              width: '100%',
              display: 'flex',
              justifyContent: 'center',
              alignItems: 'center',
              padding: '20px 0',
            }}
          ></div>
        </div>

        <InformativeModal
          userName={name}
          hideModal={hideModal}
          validationData={classModal ? 'followings' : 'followers'}
          active={modal}
          renderArr={classModal}
        />
      </div>
    </>
  );
};

export default UserProfileHeader;
