import {signup} from "../../store/actions/userActions";
import M from "materialize-css";


export const PostDataFetch = async (name, password, email, setLoad, dispatch, history) => {
    setLoad(true)
    await localStorage.removeItem('app_state');
    await dispatch(signup(name, password, email));
    setLoad(false)
    if (localStorage.getItem('newUser')) {
       await setLoad(false)
      await  history.push('/signin');
        M.toast({
            html: 'Активируйте аккаунт на почте',
            classes: '#388e3c green darken-2',
        });
    }
    localStorage.removeItem('newUser');
}