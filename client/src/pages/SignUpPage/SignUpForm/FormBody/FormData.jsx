import React from 'react';
import "../../SingUp.scss"

const FormData = ({data}) => {
    return (
        <>
            <div style={{ display: 'flex', justifyContent: 'center' }}>
                <a
                    onClick={data}
                    className="waves-effect #64b5f6 blue lighten-2 btn-small"
                >
                    Регистрация
                </a>
            </div>
        </>
    );
};

export default FormData;