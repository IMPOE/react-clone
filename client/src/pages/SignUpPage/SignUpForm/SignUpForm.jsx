import React from 'react';
import SignUpFiltr from "./SignUpFilter/SignUpFiltr";
import FormData from "./FormBody/FormData";

const SignUpForm = ({
  name,
  email,
  password,
  setName,
  setEmail,
  setPassword,
  PostData,
}) => {
  return (
    <form className="col s12">
        <SignUpFiltr value={name} setName={setName} label="Имя" id="named" type="text"/>
        <SignUpFiltr value={email} setName={setEmail} label="Email" id="email" type="email"/>
        <SignUpFiltr value={password} setName={setPassword} label="Password" id="password" type="password"/>
        <FormData data={PostData}/>
      <p style={{ textAlign: 'center' }}>
        Регистрируясь, вы принимаете наши Условия, Политику использования данных
        и Политику в отношении файлов cookie.
      </p>
    </form>
  );
};

export default SignUpForm;
