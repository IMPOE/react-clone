import React from 'react';

const SignUpFiltr = ({value,setName, label,id,type}) => {
    return (
        <>
            <div className="row">
                <div className="input-field col s12">
                    <input
                        autoFocus={true}
                        style={{ zIndex: '2' }}
                        value={value}
                        onChange={(e) => setName(e.target.value)}
                        id={id}
                        type={type}
                        className="validate"
                    ></input>
                    <label htmlFor={label}>{label}</label>
                </div>
            </div>
        </>
    );
};

export default SignUpFiltr;