import React from 'react';
import {Link} from "react-router-dom";
import "../SingUp.scss"
const SignUpFooter = () => {
    return (
        <>
            <div className="log_sign">
                <p className="log_sign-text">
                    Есть аккаунт?
                    <Link
                        className="log_sign-link"
                        to={{
                            pathname: '/signin',
                        }}
                    >
                        Вход
                    </Link>
                </p>
            </div>
        </>
    );
};

export default SignUpFooter;