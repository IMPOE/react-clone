import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';
import '../LoginPage/Login.scss';
import { useDispatch } from 'react-redux';
import SignUpForm from './SignUpForm/SignUpForm';
import Loading from "../../components/Loader";
import SignUpFooter from "./SignUpFooter/SignUpFooter";
import {PostDataFetch} from "./PostData";
const SignUp = () => {
  const dispatch = useDispatch();
  const history = useHistory();
  const [name, setName] = useState('');
  const [password, setPassword] = useState('');
  const [email, setEmail] = useState('');
  const [load , setLoad] = useState(false)
  const PostData = async () => {
   await PostDataFetch(name,password,email,setLoad,dispatch,history)
  };
  return (
    <>
      { load ? <Loading/> :
          <div className="login_auth-signup">
            <div className="login_auth">
              <div className="login__form">
                <div className="row">
                  <div className="row__container">
                    <img src="https://svgsilh.com/svg/1594387.svg" alt=""/>
                  </div>
                  <p>
                    Зарегистрируйтесь, чтобы смотреть фото и видео ваших друзей.
                  </p>
                  <SignUpForm
                      name={name}
                      email={email}
                      password={password}
                      setName={setName}
                      setEmail={setEmail}
                      setPassword={setPassword}
                      PostData={PostData}
                  />
                </div>
              </div>
            </div>
            <SignUpFooter/>
          </div>
      }
    </>
  );
};

export default SignUp;
