import { combineReducers } from 'redux';
import { userReducer } from './userReducer';
import { PostReducer } from './postReducer';
import { modalReducer } from './modalReducer';
import { subscribeReducer } from './subscribeReucer';

export const rootReducer = combineReducers({
  user: userReducer,
  post: PostReducer,
  modal: modalReducer,
  subscribe: subscribeReducer,
});
