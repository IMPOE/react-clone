import { FOLLOW_USER, UNFOLLOW_USER } from '../actions/subscribeAction';

const initialState = {};

export const subscribeReducer = (state = initialState, action) => {
  switch (action.type) {
    case FOLLOW_USER:
      return {
        ...state,
        followers: action.payload.followers,
        following: action.payload.following,
      };
    case UNFOLLOW_USER:
      return {
        ...state,
        followers: action.payload.followers,
        following: action.payload.following,
      };
    default:
      return state;
  }
};
