import { SHOW_MODAL, HIDE_MODAL } from '../actions/modalAction';

const initialState = {
  isOpen: false,
};

export const modalReducer = (state = initialState, action) => {
  switch (action.type) {
    case SHOW_MODAL:
      return {
        isOpen: action.payload,
      };
    case HIDE_MODAL:
      return {
        isOpen: action.payload,
      };
    default:
      return state;
  }
};
