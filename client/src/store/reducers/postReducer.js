import {
  GET_POST,
  CREATE_POST_ACTION,
  LIKE_POST_ACTION,
  DISLIKE_POST_ACTION,
  LOGOUT_POST,
  COMMENT_POST_ACTION,
  GET_ALL_POST,
  MODAL_POST,
  GET_SUB_POST,
  SAVE_POST_ACTION,
} from '../actions/postActions';

const InitialPost = {
  data: {
    post: [],
  },
  modalPost: {
    userPost: {
      likes: [],
    },
  },
  subPost: {
    posts: [],
  },
  savedPost: [],
};

export const PostReducer = (post = InitialPost, action) => {
  switch (action.type) {
    case GET_POST:
      return { ...post, data: action.payload };
    case MODAL_POST:
      return { ...post, modalPost: action.payload };
    case GET_ALL_POST:
      return { ...post, allPost: action.payload };
    case GET_SUB_POST:
      return { ...post, subPost: action.payload };
    case SAVE_POST_ACTION:
      return { ...post, savedPost: action.payload };
    case LOGOUT_POST:
      return {
        ...post,
        data: {
          post: [],
        },
      };
    case CREATE_POST_ACTION:
      return { ...post };
    case LIKE_POST_ACTION:
      return { ...post };
    case DISLIKE_POST_ACTION:
      return { ...post };
    case COMMENT_POST_ACTION:
      return { ...post };
    default:
      return post;
  }
};
