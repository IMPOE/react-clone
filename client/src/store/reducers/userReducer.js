import {
  GET_RESET_TOKEN_USER,
  GET_USER,
  GET_USER_DATA,
  GET_USER_FEED,
  GET_USER_PHOTO,
  GET_USER_RECOMMENDS,
  GET_USER_SUBSCRIBES,
  GET_USERS,
  LOGOUT_USER,
  SEARCH_USER,
  UPDATE_USER_IMG,
} from '../actions/userActions';

const InitialUser = {
  isActivated: false,
  search: {
    data: {
      user: [],
    },
  },
  followers: [],
  following: [],
  photos: [],
};

export const userReducer = (user = InitialUser, action) => {
  switch (action.type) {
    case GET_USER_DATA:
      return { ...user, ...action.payload };
    case GET_USER:
      return { ...user, ...action.payload };
    case SEARCH_USER:
      return { ...user, search: action.payload };
    case GET_USERS:
      return { ...user, subscribes: action.payload };
    case LOGOUT_USER:
      return { ...user, isActivated: false };
    case GET_USER_PHOTO:
      const pic = action.payload;

      const photos = user.photos;
      photos.push(pic);

      return { ...user };
    case GET_RESET_TOKEN_USER:
      return { ...user, resetToken: action.payload };
    case GET_USER_FEED:
      return { ...user, feed: action.payload };
    case GET_USER_SUBSCRIBES:
      return { ...user, subscribes: action.payload };
    case GET_USER_RECOMMENDS:
      return { ...user, recommends: action.payload };
    case UPDATE_USER_IMG:
      return { ...user, img: action.payload };
    default:
      return user;
  }
};
