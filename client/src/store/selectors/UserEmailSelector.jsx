const userEmailSelector = (store) => store.user.email

export default userEmailSelector