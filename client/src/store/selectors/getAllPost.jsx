const getAllPostAction = (store) => store.post.allPost;

export default getAllPostAction;
