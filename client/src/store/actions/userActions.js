import api from '../../utils/api.js';
import M from 'materialize-css';
import { LOGOUT_POST } from './postActions';
import { followUserAction } from './subscribeAction';
export const GET_USER = 'GET_USER';
export const GET_USER_DATA = 'GET_USER_DATA';
export const GET_USERS = 'GET_USERS';
export const LOGOUT_USER = 'LOGOUT_USER';
export const GET_USER_FEED = 'GET_USER_FEED';
export const GET_USER_SUBSCRIBES = 'GET_USER_SUBSCRIBES';
export const GET_USER_RECOMMENDS = 'GET_USER_RECOMMENDS';
export const UPDATE_USER_IMG = 'UPDATE_USER_IMG';
export const GET_RESET_TOKEN_USER = 'GET_RESET_TOKEN_USER';
export const SEARCH_USER = 'SEARCH_USER';
export const GET_USER_PHOTO = 'GET_USER_PHOTO';

export const login = (password, email) => async (dispatch) => {
  await api
    .post('/signin', {
      password,
      email,
    })
    .then((response) => {
      if (response.data.user.isActivated === false) {
        M.toast({
          html: 'activate link on your mail',
          classes: '#c62828 red darken-3',
        });
        return false;
      }
      if (response.data.user.isActivated === true) {
        localStorage.setItem('jwt', `${response.data.accessToken}`);
        localStorage.setItem('jwtR', `${response.data.refreshToken}`);
        dispatch({ type: GET_USER, payload: response.data.user });
        M.toast({ html: 'Logged in', classes: '#388e3c green darken-2' });
        dispatch(tokenPassword());
      }
    })
    .catch((error) => {
      console.log(error.messages);
      M.toast({
        html: 'Check password or email',
        classes: '#c62828 red darken-3',
      });
    });
};

export const getUser = () => async (dispatch) => {
  await api
    .get('/userdata')
    .then((resp) => {
      const response = resp.data;
      if (response.status === 'error') {
        console.error(response.message);
      } else {
        dispatch({ type: GET_USER_DATA, payload: response });
      }
    })
    .catch((err) => {
      console.log(err);
    });
};
export const getUserInfo =
  (username, setProfileData, setPostsData) => async (dispatch) => {
    await api
      .get(`/user/${username}`)
      .then((resp) => {
        const response = resp.data;
        if (response.status === 'error') {
          console.error(response.message);
        } else {
          setProfileData(response[0]);
          setPostsData(response[1]);
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

export const signup = (name, password, email) => async (dispatch) => {
    try {
        await api
            .post('/signup', {
                name,
                password,
                email,
            })
            .then(function (response ) {
                if (response.status === 200) {
                    localStorage.setItem('newUser', 'true');
                    localStorage.setItem('name', response.data.user.name);
                    localStorage.setItem('jwt', response.data.accessToken);
                }
            })
            .then((el) => console.log(el))
    } catch (e) {
                 return M.toast({
                 html: e.response.data.message,
                  classes: '#c62828 red darken-3',
                 });
    }
};

export const searchUser = (query) => async (dispatch) => {
  await api
    .post('/search-users', {
      query: query,
    })
    .then(function (response) {
      dispatch({ type: SEARCH_USER, payload: response });
    })
    .catch((err) => {
      console.log(err);
    });
};

export const clearUser = () => (dispatch) => {
  localStorage.removeItem('jwt');
  dispatch({
    type: LOGOUT_USER,
    payload: {
      isActivated: false,
    },
  });
  dispatch({
    type: LOGOUT_POST,
    payload: {
      data: {
        post: [],
      },
    },
  });
};

export const logout = () => async (dispatch) => {
  await api
    .post('/logout', {})
    .then(function (response) {
      localStorage.removeItem('jwt');
      dispatch({
        type: LOGOUT_USER,
        payload: {
          isActivated: false,
        },
      });
      dispatch({
        type: LOGOUT_POST,
        payload: {
          data: {
            post: [],
          },
        },
      });
    })
    .catch((err) => {
      console.log(err);
    });
};

export const getUserAction = () => async (dispatch) => {
  await api
    .get('/users')
    .then(async (resp) => {
      const response = resp.data;
      if (response.status === 'error') {
        console.error(response.message);
      } else {
        dispatch({ type: GET_USERS, payload: response });
      }
    })
    .catch((err) => {
      console.log(err);
    });
};

export const getUserActionForModal =
  (setUserFollowers, setUserFollowing, userData) => async () => {
    await api
      .get('/users')
      .then(async (resp) => {
        const response = resp.data;
        if (response.status === 'error') {
          console.error(response.message);
        } else {
          const followers = response.filter((user) => {
            return user.followers.includes(userData._id);
          });
          await setUserFollowers(followers);

          const following = response.filter((user) => {
            return user.following.includes(userData._id);
          });
          await setUserFollowing(following);
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

export const getUserFeedAction = (userId) => async (dispatch) => {
  await api
    .get(`/users/${userId}/feed`)
    .then((resp) => {
      const response = resp.data;
      if (response.status === 'error') {
        console.error(response.message);
      } else {
        dispatch({ type: GET_USER_FEED, payload: response });
      }
    })
    .catch((err) => {
      console.log(err);
    })
    .catch((err) => {
      console.log(err);
    });
};

export const getPassword = (email) => async (dispatch) => {
  await api
    .post(`/forgotpassword`, { email: email })
    .then((resp) => {
      if (resp.status === 400) {
        console.error(resp.data.message);
      } else {
        if (resp.status === 200) {
          localStorage.setItem('changePassword', 'true');
          M.toast({
            html: 'check your email',
            classes: '#008000 green darken-3',
          });
        }
      }
    })
    .catch((err) => {
      return M.toast({
        html: 'User not found',
        classes: '#c62828 red darken-3',
      });
    });
};

export const getUserSubscribesAction = (userId) => async (dispatch) => {
  await api
    .get(`/users/${userId}/subscribes`)
    .then((resp) => {
      const response = resp.data;
      if (response.status === 'error') {
        console.error(response.message);
      } else {
        dispatch({ type: GET_USER_SUBSCRIBES, payload: response });
      }
    })
    .catch((err) => {
      console.log(err);
    });
};

export const addUserSubscribeAction =
  (userId, subscribeId) => async (dispatch) => {
    await api
      .post(`/users/${userId}/subscribes`, { subscribeId })
      .then((resp) => {
        const response = resp.data;
        if (response.status === 'error') {
          console.error(response.message);
        } else {
          dispatch(getUserFeedAction(userId));
          dispatch(getUserSubscribesAction(userId));
          dispatch(getUserRecommendsAction(userId));
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

export const newPassword = (password, resetToken) => async (dispatch) => {
  await api
    .post(`/new-password`, { password: password, resetToken: resetToken })
    .then(async (response) => {
      if (response.status === 200) {
        await localStorage.setItem('newPassword', 'true');
        M.toast({
          html: 'successes changed ',
          classes: '#008000 green darken-3',
        });
      }
    })
    .catch((err) => {
      console.log(err);
    });
};

export const tokenPassword = () => async (dispatch) => {
  await api
    .post(`/reset-password`)
    .then((response) => {
      dispatch({
        type: GET_RESET_TOKEN_USER,
        payload: response.data.resetToken,
      });
    })
    .catch((err) => {
      console.log(err);
    })
    .catch((err) => {
      console.log(err);
    });
};

export const removeUserSubscribeAction =
  (userId, subscribeId) => async (dispatch) => {
    await api
      .delete(`/users/${userId}/subscribes`, { subscribeId })
      .then((resp) => {
        const response = resp.data;
        if (response.status === 'error') {
          console.error(response.message);
        } else {
          dispatch(getUserFeedAction(userId));
          dispatch(getUserSubscribesAction(userId));
          dispatch(getUserRecommendsAction(userId));
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

export const getUserRecommendsAction = (userId) => async (dispatch) => {
  await api
    .get(`/users/${userId}/recommends`)
    .then((resp) => {
      const response = resp.data;
      if (response.status === 'error') {
        console.error(response.message);
      } else {
        dispatch({ type: GET_USER_RECOMMENDS, payload: response });
      }
    })
    .catch((err) => {
      console.log(err);
    });
};

export const changePhoto = (url) => async (dispatch) => {
  await api
    .post(`/changephoto`, { pic: url })
    .then((resp) => {
      const response = resp.data;
      if (response.status === 'error') {
        console.error(response.message);
      } else {
        dispatch({ type: GET_USER, payload: response.user });
      }
    })
    .catch((err) => {
      console.log(err);
    });
};

export const deletePhotos = () => async (dispatch) => {
  await api
    .post(`/deletphoto`)
    .then((resp) => {
      const response = resp.data;
      if (response.status === 'error') {
        console.error(response.message);
      } else {
        dispatch({ type: GET_USER, payload: response.user });
      }
    })
    .catch((err) => {
      console.log(err);
    });
};

export const getUsersPhoto = (userId) => async (dispatch) => {
  await api
    .post(`/getphoto`, { userId: userId })
    .then((resp) => {
      const response = resp.data;
      if (response.status === 'error') {
        console.error(response.message);
      } else {
        dispatch({ type: GET_USER_PHOTO, payload: response.user });
      }
    })
    .catch((err) => {
      console.log(err);
    });
};

export const follow = (setProfileData) => async (dispatch) => {
  await api
    .put(`/follow`)
    .then((resp) => {
      const response = resp.data;
      if (response.status === 'error') {
        console.error(response.message);
      } else {
        localStorage.setItem('app_state', JSON.stringify(response));
        dispatch(followUserAction(response));
        setProfileData((prevState) => {
          return {
            ...prevState,
            followers: [...prevState.followers, response._id],
          };
        });
      }
    })
    .catch((err) => {
      console.log(err);
    });
};
