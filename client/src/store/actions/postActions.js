import api from '../../utils/api.js';
import M from 'materialize-css';
export const GET_POST = 'GET_POST';
export const GET_ALL_POST = 'GET_ALL_POST';
export const LIKE_POST_ACTION = 'LIKE_POST_ACTION';
export const CREATE_POST_ACTION = 'CREATE_POST_ACTION';
export const DISLIKE_POST_ACTION = 'DISLIKE_POST_ACTION';
export const COMMENT_POST_ACTION = 'COMMENT_POST_ACTION';
export const LOGOUT_POST = 'LOGOUT_POST';
export const MODAL_POST = 'MODAL_POST';
export const GET_SUB_POST = 'GET_SUB_POST';
export const SAVE_POST_ACTION = 'SAVE_POST_ACTION';

export const getPosts = () => async (dispatch) => {
  await api
    .get(`/mypost`)
    .then((resp) => {
      const response = resp.data;
      if (response.status === 'error') {
        console.error(response.message);
        return (resp.data = []);
      } else {
        dispatch({ type: GET_POST, payload: Object.assign(resp.data) });
      }
    })
    .catch((err) => {
      console.log(err);
    });
};

export const deletePost = (postId) => async (dispatch) => {
  await api
    .delete(`/deletepost`, { postId: postId })
    .then((resp) => {
      const response = resp.data;
      const massege = resp.data.err;
      if (response.status === 'error') {
        console.error(response.message);
      } else {
        if (response.post) {
          M.toast({ html: 'Post deleted', classes: '#008000 green darken-3' });
        } else {
          M.toast({ html: `${massege}`, classes: '#c62828 red darken-3' });
        }
      }
    })
    .catch((err) => {
      console.log(err);
    });
};

export const getSubPost = (following) => async (dispatch) => {
  await api
    .put(`/getsubpost`, { following: following })
    .then((resp) => {
      const response = resp.data;
      if (response.status === 'error') {
        console.error(response.message);
      } else {
        dispatch({ type: GET_SUB_POST, payload: resp.data });
      }
    })
    .catch((err) => {
      console.log(err);
    });
};

export const getAllPosts = () => async (dispatch) => {
  await api
    .get(`/allpost`)
    .then((resp) => {
      const response = resp.data;
      if (response.status === 'error') {
        console.error(response.message);
      } else {
        dispatch({ type: GET_ALL_POST, payload: Object.assign(resp.data) });
      }
    })
    .catch((err) => {
      console.log(err);
    });
};

export const findePost = (postId) => async (dispatch) => {
  await api
    .get(`/onepost`, { postId: postId })
    .then((resp) => {
      const response = resp.data;
      if (response.status === 'error') {
        console.error(response.message);
      } else {
        dispatch({ type: MODAL_POST, payload: resp.data });
      }
    })
    .catch((err) => {
      console.log(err);
    });
};

export const createPostAction = (title, body, url) => async (dispatch) => {
  await api
    .post(`/createpost`, { title, body, pic: url })
    .then((resp) => {
      const response = resp.data;
      if (response.status === 'error') {
        console.error(response.message);
      } else {
        dispatch({ type: CREATE_POST_ACTION, payload: response });
      }
    })
    .catch((err) => {
      console.log(err);
    });
};

export const savePostAction = (userId, postId) => async (dispatch) => {
  await api
    .put(`/savedpost`, { postId: postId, userId: userId })
    .then((resp) => {
      const response = resp.data;
      if (response.status === 'error') {
        console.error(response.message);
      } else {
        M.toast({ html: 'Post saved ', classes: '#008000 green darken-3' });
      }
    })
    .catch((err) => {
      console.log(err);
    });
};

export const likePostAction =
  (postId, userId, name, pic) => async (dispatch) => {
    await api
      .put(`/like`, { postId: postId, userId: userId, name: name, pic: pic })
      .then((resp) => {
        const response = resp.data;
        if (response.status === 'error') {
          console.error(response.message);
        } else {
          dispatch({ type: LIKE_POST_ACTION, payload: response });
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

export const onePost = (postId) => async (dispatch) => {
  await api
    .post(`/onepost`, {
      postId: postId,
    })
    .then((resp) => {
      if (resp.status === 'error') {
        console.error(resp.message);
      } else {
        dispatch({ type: MODAL_POST, payload: resp.data });
      }
    })
    .catch((err) => {
      console.log(err);
    });
};

export const dislikePostAction =
  (postId, userId, name, pic) => async (dispatch) => {
    await api
      .put(`/unlike`, { postId: postId, userId: userId, name: name, pic: pic })
      .then((resp) => {
        if (resp.status === 'error') {
          console.error(resp.message);
        } else {
          dispatch({ type: DISLIKE_POST_ACTION, payload: resp });
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

export const commentPostAction = (data, onComplete) => async (dispatch) => {
  await api
    .put(`/comment`, { data: data })
    .then(async (resp) => {
      onComplete();
      const response = resp.data;
      if (response.status === 'error') {
        console.error(response.message);
      } else {
        await dispatch({ type: COMMENT_POST_ACTION, payload: response });
      }
    })
    .catch((err) => {
      console.log(err);
    });
};

export const getSavedPostUsers = (userId) => async (dispatch) => {
  await api
    .put(`/getsavedpost`, { userId: userId })
    .then(async (resp) => {
      const response = resp.data;
      if (response.status === 'error') {
        console.error(response.message);
      } else {
        await dispatch({ type: 'SAVE_POST_ACTION', payload: response });
      }
    })
    .catch((err) => {
      console.log(err);
    });
};
