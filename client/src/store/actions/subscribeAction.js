export const FOLLOW_USER = 'FOLLOW_USER';
export const UNFOLLOW_USER = 'UNFOLLOW_USER';

export const followUserAction = (data) => (dispatch) => {
  dispatch({
    type: FOLLOW_USER,
    payload: {
      followers: data.followers,
      following: data.following,
    },
  });
};

export const unfollowUserAction = (data) => (dispatch) => {
  dispatch({
    type: UNFOLLOW_USER,
    payload: {
      followers: data.followers,
      following: data.following,
    },
  });
};
