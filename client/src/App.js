import React from 'react';
import {useSelector} from 'react-redux';
import Router from './routes/Router';
import Header from './components/Header/Header';

const App = () => {
    const auth = useSelector((state) => state.user.isActivated) || false;
    const email = useSelector((state) => state.user.email);
    return (
        <div>{auth && <Header email={email}/>}
            <Router/>
        </div>
    );
};

export default App;
