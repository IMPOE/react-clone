import React from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';
import Page404 from '../pages/Page404/Page404';
import SignUp from '../pages/SignUpPage/SignUp';
import Auth from '../pages/Authentication/Auth';
import ChangePassword from '../pages/ChangePassword/ChangePassword';
import MainOne from '../pages/Main/MainOne';
import CreatePost from '../components/CreatePost/CreatePost';
import ProtectedRoute from './ProtectedRoute';
import ForgotPassword from '../pages/ForgotPassword/ForgotPassword';
import UserProfile from '../pages/UserProfile/UserProfile';
import { Profile } from '../pages/ProfilePage/Profile';
import StandardImageList from '../pages/Explore/Explore';
import SavedPost from '../pages/SavedPost/SavedPost';
import EditProfile from '../pages/EditeProfile/EditProfile';
export default function Router() {
  return (
    <Switch>
      <Route exact path={'/signup'}>
        <SignUp />
      </Route>
      <ProtectedRoute exact path={'/home'}>
        <MainOne />
      </ProtectedRoute>
      <Route exact path={'/forgotpassword'}>
        <ForgotPassword />
      </Route>
      <ProtectedRoute exact path={'/createpost'}>
        <CreatePost />
      </ProtectedRoute>
      <ProtectedRoute exact path={'/editgprofile'}>
        <EditProfile />
      </ProtectedRoute>
      <ProtectedRoute exact path={'/savedpost'}>
        <SavedPost />
      </ProtectedRoute>
      <ProtectedRoute exact path={'/find-people'}>
        <StandardImageList />
      </ProtectedRoute>
      <ProtectedRoute exact path={'/profile'}>
        <Profile />
      </ProtectedRoute>
      <ProtectedRoute path={'/profile/:username'}>
        <UserProfile />
      </ProtectedRoute>
      <Route exact path={'/signin'}>
        <Auth />
      </Route>
      <ProtectedRoute exact path={'/new-password'}>
        <ChangePassword />
      </ProtectedRoute>
      <Redirect from={'*'} to={'/home'} />
      <ProtectedRoute path="*">
        <Page404 />
      </ProtectedRoute>
      <Redirect from={'/'} to={'/signin'} />
    </Switch>
  );
}
