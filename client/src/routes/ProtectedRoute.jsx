import React from 'react';
import { Redirect, Route } from 'react-router-dom';
const ProtectedRoute = (routerProps) => {
  const token = localStorage.getItem('jwt');

  return token ? <Route {...routerProps} /> : <Redirect to={'/signin'} />;
};

export default ProtectedRoute;
