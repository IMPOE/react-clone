import './loading.scss';
import {CircularProgress} from "@material-ui/core";

const Loading = () => {
  return (
    <div className="loading-container">
      <h5 className="loading-container__item">
        Wait for few seconds. Loading...
          <CircularProgress />
      </h5>
    </div>
  );
};

export default Loading;
