import React, { useState } from 'react';
import { withStyles } from '@material-ui/core/styles';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import { Link, useHistory } from 'react-router-dom';
import SettingsIcon from '@material-ui/icons/Settings';
import LockOpenIcon from '@material-ui/icons/LockOpen';
import BookmarkIcon from '@material-ui/icons/Bookmark';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import { useDispatch } from 'react-redux';
import { logout } from '../../store/actions/userActions';
const StyledMenu = withStyles({
  paper: {
    $border: '1px solid #d3d4d5',
  },
})((props) => (
  <Menu
    elevation={0 ? 0 : 1}
    getContentAnchorEl={null}
    anchorOrigin={{
      $vertical: 'bottom',
      $horizontal: 'center',
    }}
    transformOrigin={{
      $vertical: 'top',
      $horizontal: 'center',
    }}
    {...props}
  />
));

const StyledMenuItem = withStyles((theme) => ({
  root: {
    '&:focus': {
      $backgroundColor: theme.palette.text.secondary,
      '& .MuiListItemIcon-root, & .MuiListItemText-primary': {
        $color: theme.palette.text.secondary,
      },
    },
  },
}))(MenuItem);

export default function CustomizedMenus() {
  const [anchorEl, setAnchorEl] = useState(null);
  const dispatch = useDispatch();
  const history = useHistory();
  const logoutUser = async () => {
    await dispatch(logout());
    setTimeout(() => {
      history.push('/signin');
    }, 100);
  };
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <div>
      <SettingsIcon
        aria-controls="customized-menu"
        aria-haspopup="true"
        variant="contained"
        color="inherit"
        onClick={handleClick}
      ></SettingsIcon>
      <StyledMenu
        id="customized-menu"
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleClose}
      >
        <Link to="/new-password">
          <StyledMenuItem>
            <ListItemIcon>
              <LockOpenIcon fontSize="small" />
            </ListItemIcon>
            <ListItemText primary="Change password" />
          </StyledMenuItem>
        </Link>
        <Link to="/savedpost">
          <StyledMenuItem>
            <ListItemIcon>
              <BookmarkIcon fontSize="small" />
            </ListItemIcon>
            <ListItemText primary="Saved Post" />
          </StyledMenuItem>
        </Link>
        <StyledMenuItem onClick={logoutUser}>
          <ListItemIcon>
            <ExitToAppIcon fontSize="small" />
          </ListItemIcon>
          <ListItemText primary="Logout" />
        </StyledMenuItem>
      </StyledMenu>
    </div>
  );
}
