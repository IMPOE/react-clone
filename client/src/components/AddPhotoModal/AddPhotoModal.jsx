import React, { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import { changePhoto, deletePhotos } from '../../store/actions/userActions';
import { makeStyles } from '@material-ui/core/styles';
import Modal from '@material-ui/core/Modal';
import Backdrop from '@material-ui/core/Backdrop';
import Fade from '@material-ui/core/Fade';
import Button from '@material-ui/core/Button';
import { Avatar } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
  modal: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  paper: {
    backgroundColor: theme.palette.background.paper,
    border: '2px solid #000',
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
  },
}));

export default function TransitionsModal({ pic }) {
  const dispatch = useDispatch();
  const classes = useStyles();
  const [open, setOpen] = React.useState(false);
  const [image, setImg] = useState('');
  const [url, setUrl] = useState('');

  useEffect(() => {
    if (url) {
      dispatch(changePhoto(url));
    }
  }, [dispatch, url]);

  const deletePhoto = () => {
    dispatch(deletePhotos());
  };

  const postDetail = () => {
    const data = new FormData();
    data.append('file', image);
    data.append('upload_preset', 'insta-clone');
    data.append('cloud_name', 'duuu2fnc1');
    fetch('https://api.cloudinary.com/v1_1/duuu2fnc1/image/upload', {
      method: 'post',
      body: data,
    })
      .then((res) => res.json())
      .then((data) => {
        setUrl(data.url);
      })
      .catch((err) => {
        console.log(err);
      });
    setOpen(false);
  };

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <div>
      <Avatar
        src={
          pic
            ? pic
            : 'https://www.pikpng.com/pngl/m/16-168770_user-iconset-no-profile-picture-icon-circle-clipart.png'
        }
        alt="logo"
        style={{ width: '12rem', height: '12rem', cursor: 'pointer' }}
        onClick={handleOpen}
      />
      <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        className={classes.modal}
        open={open}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        <Fade in={open}>
          <div
            className={classes.paper}
            style={{
              height: '12rem',
              display: 'flex',
              flexDirection: 'column',
              alignItems: 'center',
            }}
          >
            <div>
              <span>Upload image</span>
              <input type="file" onChange={(e) => setImg(e.target.files[0])} />
            </div>
            <Button
              onClick={postDetail}
              style={{
                textAlign: 'center',
                marginTop: '1rem',
                backgroundColor: 'gray',
              }}
              size="large"
            >
              Submit post
            </Button>
            <hr
              style={{
                width: '24rem',
                height: '0.5px',
                color: 'black',
                backgroundColor: 'grey',
                marginTop: '1rem',
              }}
            />
            <Button
              onClick={deletePhoto}
              style={{
                textAlign: 'center',
                marginTop: '1rem',
                backgroundColor: 'red',
              }}
              size="small"
            >
              Delete photo
            </Button>
          </div>
        </Fade>
      </Modal>
    </div>
  );
}
