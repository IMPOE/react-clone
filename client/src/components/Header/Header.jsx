import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import './Header.scss';
import { Avatar, Tooltip } from '@material-ui/core';
import { searchUser } from '../../store/actions/userActions';
import HeaderMenu from './HeaderMenu/HeaderMunu';
const Header = () => {
  const dispatch = useDispatch();
  const search = useSelector((state) => state.user.search.data.user) || [];
  const [query, setQuery] = useState('');
  const userOwnData = useSelector((state) => state.user);
  const [searchMod, setSearchMod] = useState(false);

  const fetchUsers = async (query) => {
    await setQuery(query);
    await dispatch(searchUser(query));
    await setSearchMod(true);
  };

  const searchParams = async () => {
    await setQuery('');
    setTimeout(async () => {
      await setSearchMod(false);
    }, 1000);
  };

  return (
    <header className="header">
      <div className="container" onClick={() => setSearchMod(false)}>
        <div className="header__inner">
          <Link to="/home">
            <div className="header__brand">
              {(
                <img
                  src="https://i.pinimg.com/originals/66/41/c9/6641c94e15a0be37af49a4250386c03e.png"
                  alt="logo"
                />
              ) || <img src="logo.png" alt="" />}
            </div>
          </Link>
          <input
            key="input"
            className="header__search"
            type="text"
            placeholder="Поиск"
            value={query}
            onBlur={() => searchParams()}
            onChange={(e) => fetchUsers(e.target.value)}
          />
          {searchMod ? (
            <div className="search__modal">
              {search.map((e) => {
                return (
                  <div className="modal__user">
                    <Tooltip title="Go to profile" arrow>
                      <Link
                        className="modal__user-link"
                        to={
                          userOwnData?.name !== e?.name
                            ? '/profile/' + e?.name
                            : '/profile'
                        }
                      >
                        <div className="modal__user-avatar">
                          <Avatar
                            className="modal__user-avatar-pic"
                            src={e?.pic}
                          />
                          <p
                            style={{ paddingBottom: '1rem' }}
                          >{`Name:  ${e?.name}`}</p>
                          <p
                            className="modal__email-user"
                            style={{ marginLeft: '8rem' }}
                          >{`Email:  ${e?.email}`}</p>
                        </div>
                      </Link>
                    </Tooltip>
                  </div>
                );
              })}
            </div>
          ) : (
            ''
          )}
          <HeaderMenu />
        </div>
      </div>
    </header>
  );
};

export default Header;
