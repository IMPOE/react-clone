import React from 'react';
import { NavLink } from 'react-router-dom';
import HomeIcon from '../../assests/HomeIcon';
import CompassIcon from '../../assests/CompasIcon';
import LikeIcon from '../../assests/LikeIcon';
import CustomizedMenus from '../../UserAsideMenue/CustomizedMenue';
import { useSelector } from 'react-redux';
import userSelector from '../../../store/selectors/userSelector';

const HeaderMenu = () => {
  const { pic, name } = useSelector(userSelector);
  return (
    <div className="header__menu">
      <NavLink activeClassName="header__menu__home-active" exact to="/home">
        <HomeIcon />
      </NavLink>
      <NavLink
        activeClassName="header__menu__find-people-active"
        exact
        to="/find-people"
      >
        <CompassIcon />
      </NavLink>
      <LikeIcon style={{ cursor: 'pointer' }} />
      <NavLink
        className="header__menu__profile__link"
        activeClassName="header_menu__profile__link--active"
        to={`/profile`}
      >
        <div className="header__menu__profile">
          <img
            src={
              pic ||
              'https://www.pikpng.com/pngl/m/16-168770_user-iconset-no-profile-picture-icon-circle-clipart.png'
            }
            alt={name}
          />
        </div>
      </NavLink>
      <CustomizedMenus />
    </div>
  );
};

export default HeaderMenu;
