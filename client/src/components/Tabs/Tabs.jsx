import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import { FormControl, Input, InputLabel } from '@material-ui/core';
import { useSelector } from 'react-redux';
import TransitionsModal from '../AddPhotoModal/AddPhotoModal';

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      style={{ marginTop: '20rem' }}
      role="tabpanel"
      hidden={value !== index}
      id={`vertical-tabpanel-${index}`}
      aria-labelledby={`vertical-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

function a11yProps(index) {
  return {
    id: `vertical-tab-${index}`,
    'aria-controls': `vertical-tabpanel-${index}`,
  };
}

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
    display: 'flex',
    height: 500,
  },
  tabs: {
    borderRight: `1px solid ${theme.palette.divider}`,
  },
}));

export default function VerticalTabs() {
  const user = useSelector((store) => store.user);
  const classes = useStyles();
  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <div className={classes.root}>
      <Tabs
        style={{ marginTop: '15rem' }}
        orientation="vertical"
        variant="scrollable"
        value={value}
        onChange={handleChange}
        aria-label="Vertical tabs example"
        className={classes.tabs}
      >
        <Tab
          style={{ marginTop: '3rem' }}
          label="Редактировать профиль"
          {...a11yProps(0)}
        />
        <Tab label="Входы в аккаунт" {...a11yProps(1)} />
        <Tab label="Электронная почта" {...a11yProps(2)} />
        <Tab label="Push-уведоления" {...a11yProps(3)} />
      </Tabs>
      <TabPanel
        value={value}
        index={0}
        style={{ width: '70%', height: '100hv', marginTop: '8rem' }}
      >
        <div
          className="edit"
          style={{ display: 'flex', justifyContent: 'center' }}
        >
          <div
            style={{
              display: 'flex',
              alignItems: 'center',
              flexDirection: 'column',
            }}
          >
            <TransitionsModal pic={user?.pic} />
            <div>
              <p>Сменить фото профиля</p>
            </div>
          </div>
        </div>
        <div style={{ display: 'flex', justifyContent: 'center' }}>
          <p
            style={{
              fontSize: '1.5em',
              paddingLeft: '8rem',
              paddingTop: '1.2rem',
            }}
          >
            Имя
          </p>
          <div style={{ marginLeft: '3rem' }}>
            <FormControl>
              <InputLabel htmlFor="input-with-icon-adornment">
                Enter you name
              </InputLabel>
              <Input id="input-with-icon-adornment" />
            </FormControl>
            <p style={{ width: '85%', color: 'gray' }}>
              Чтобы людям было проще находить ваш аккаунт, используйте имя, под
              которым вас знают: ваше имя и фамилию, никнейм или название
              компании.
            </p>

            <p style={{ width: '85%', color: 'gray' }}>
              Изменить имя можно не более двух раз в течение 14 дней.
            </p>
          </div>
        </div>
        <div
          style={{
            display: 'flex',
            justifyContent: 'center',
            marginLeft: '6.5rem',
          }}
        >
          <div
            style={{ textAlign: 'right', position: 'relative', right: '6rem' }}
          >
            <p style={{ fontSize: '1.5em', lineHeight: '10px' }}>Имя</p>
            <p style={{ fontSize: '1.5em', lineHeight: '12px' }}>
              Пользователя
            </p>
          </div>
          <div>
            <FormControl style={{ position: 'relative', right: '2.8rem' }}>
              <InputLabel htmlFor="input-with-icon-adornment">
                Enter you name
              </InputLabel>
              <Input id="input-with-icon-adornment" />
            </FormControl>
            <p
              style={{
                width: '85%',
                color: 'gray',
                position: 'relative',
                right: '2.8rem',
              }}
            >
              В большинстве случаев у вас будет ещё 14 дней, чтобы снова
              поменять имя пользователя на ...
            </p>
          </div>
        </div>
      </TabPanel>
      <TabPanel value={value} index={1}>
        Item Two
      </TabPanel>
      <TabPanel value={value} index={2}>
        Item Three
      </TabPanel>
      <TabPanel value={value} index={3}>
        Item Four
      </TabPanel>
    </div>
  );
}
