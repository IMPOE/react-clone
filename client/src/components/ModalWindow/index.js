import {
  ModalHeader,
  ModalBody,
  ModalWindow,
  ModalContent,
  CloseBtn,
  Close,
  ModalFooter,
} from '../../styled-components/modalStyles';
import '../../App.css';
import { Link } from 'react-router-dom';
import './Modal.scss';
import { Avatar } from '@material-ui/core';
import { useSelector } from 'react-redux';

function Modal(props) {
  const {
    active,
    onModalClose,
    header,
    subText,
    id,
    footer,
    comments = [],
    postUrl,
  } = props;

  const userOwnData = useSelector((state) => state.user);

  const userComment = comments.map(({ text, data }) => {
    return (
      <div className="modal__content" style={{ zIndexndex: '3' }}>
        <div className="modal__content-box">
          <Avatar
            src={data?.pic}
            className="modal__content-avatar"
            alt={data?.text}
          />
          <Link
            className={'link'}
            onClick={() => {
              onModalClose();
            }}
            to={
              userOwnData.name !== data?.name
                ? '/profile/' + data?.name
                : '/profile'
            }
          >
            <p className="modal__content-text">{data ? data?.name : ''}</p>
          </Link>
        </div>
        <div className="modal__content-description">
          <p className="modal__content-description-text">{text ? text : ''}</p>
        </div>
      </div>
    );
  });

  return (
    <ModalWindow
      className={active ? 'modal active' : 'modal '}
      onClick={onModalClose}
      id={id}
    >
      <ModalContent
        className={active ? 'modal__content active' : 'modal__content'}
        onClick={(e) => {
          e.stopPropagation();
        }}
      >
        <ModalHeader className="modal-header">
          <h5 className="modal-title">{header}</h5>
          <CloseBtn>
            <Close onClick={onModalClose}>&times;</Close>
          </CloseBtn>
        </ModalHeader>
        <ModalBody
          className="modal-body"
          style={{
            height: '25rem',
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
          }}
        >
          <div
            className="body__contant"
            style={{
              display: 'flex',
              justifyContent: 'space-between',
              height: '20rem',
            }}
          >
            <img
              src={postUrl ? postUrl : ''}
              alt="post"
              style={{
                width: '20rem',
                height: '20rem',
                objectFit: 'cover',
                margin: '0 1rem',
              }}
            />
            <div className="comment__container">
              {userComment ? userComment : subText}
            </div>
          </div>
        </ModalBody>
        <ModalFooter className="modal-footer">{footer}</ModalFooter>
      </ModalContent>
    </ModalWindow>
  );
}

export default Modal;
