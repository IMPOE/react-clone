import React from 'react';
import LikedIcon from '../../../assests/LikedIcon';
import { Field, Form, Formik } from 'formik';
import { object, string } from 'yup';
import {
  commentPostAction,
  dislikePostAction,
  getPosts,
  likePostAction,
} from '../../../../store/actions/postActions';
import { useDispatch } from 'react-redux';
import '../../Modal.scss';
const ModalFooter = ({
  isLiked,
  modalPost,
  postId,
  pic,
  name,
  showModal,
  postFormodal,
  setUserLikedColor,
  id,
}) => {
  const dispatch = useDispatch();
  const initialValue = {
    comment: '',
  };

  const validationSchema = object({
    comment: string().min(1).required(),
  });

  const likePostHandle = (post) => {
    showModal(modalPost);
    if (isLiked.length > 0) {
      return (
        dispatch(dislikePostAction(postFormodal._id, id, name, pic)),
        setTimeout(async () => {
          await dispatch(getPosts());
          await setUserLikedColor(false);
          await showModal(modalPost);
        }, 600)
      );
    } else
      return (
        dispatch(likePostAction(postFormodal._id, id, name, pic)),
        setTimeout(async () => {
          await dispatch(getPosts());
          await setUserLikedColor(true);
          await showModal(modalPost);
        }, 600)
      );
  };

  const commentSubmitHandler = async (
    { comment },
    { setSubmitting, resetForm }
  ) => {
    const onComplete = () => {
      resetForm({ values: '' });
      setSubmitting(false);
    };
    await dispatch(
      commentPostAction({ postId, text: comment, name, pic }, onComplete)
    );
    showModal(modalPost);
  };

  return (
    <div className="footer__modal">
      <div className="footer__modal-icon">
        <LikedIcon
          style={{ marginLeft: '2rem' }}
          color={isLiked?.length === 1 ? 'red' : 'black'}
          onClick={() => likePostHandle(modalPost)}
        />
        <p>{modalPost ? modalPost?.likes.length : 0} отметок "Нравится"</p>
      </div>
      <Formik
        initialValues={initialValue}
        validationSchema={validationSchema}
        onSubmit={commentSubmitHandler}
      >
        {({ isSubmitting }) => (
          <Form>
            <Field
              id="sub__modal"
              name="comment"
              type="text"
              disabled={isSubmitting}
              placeholder="Add comment..."
            />
            <button
              id="field__btn"
              className="field__btn"
              type="submit"
              disabled={isSubmitting}
            >
              Send
            </button>
          </Form>
        )}
      </Formik>
    </div>
  );
};

export default ModalFooter;
