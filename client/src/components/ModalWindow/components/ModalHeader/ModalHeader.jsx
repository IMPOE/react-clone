import React from 'react';
import { Avatar } from '@material-ui/core';
import './style.scss';

const ModalHeader = ({ pic, name, postFormodal }) => {
  return postFormodal ? (
    <div className="modal__header-container">
      <Avatar className="modal__header-container-avatar" src={pic} alt="logo" />
      <p style={{ paddingTop: -10 }}>{name}</p>
    </div>
  ) : (
    <div className="modal__header-container">
      <Avatar className="modal__header-container-avatar" src={pic} alt="logo" />
      <p style={{ paddingTop: -10 }}>{name ? name : ''}</p>
    </div>
  );
};

export default ModalHeader;
