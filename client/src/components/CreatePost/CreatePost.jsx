import React, { useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import { TextField } from '@material-ui/core';
import { createPostAction, getPosts } from '../../store/actions/postActions';
import { useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';

const useStyles = makeStyles({
  root: {
    minWidth: 275,
  },
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)',
  },
  title: {
    fontSize: 14,
  },
  pos: {
    marginBottom: 12,
  },
});

export default function CreatePost() {
  const dispatch = useDispatch();
  const history = useHistory();
  const [title, setTitle] = useState('');
  const [body, setBody] = useState('');
  const [image, setImg] = useState('');
  const [url, setUrl] = useState('');

  useEffect(() => {
    if (url) {
      dispatch(createPostAction(title, body, url));
      dispatch(getPosts());
      history.push('/profile');
    }
  }, [dispatch, url]);

  const postDetail = () => {
    const data = new FormData();
    data.append('file', image);
    data.append('upload_preset', 'insta-clone');
    data.append('cloud_name', 'duuu2fnc1');
    fetch('https://api.cloudinary.com/v1_1/duuu2fnc1/image/upload', {
      method: 'post',
      body: data,
    })
      .then((res) => res.json())
      .then((data) => {
        setUrl(data?.url);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const classes = useStyles();

  return (
    <div
      className="card"
      style={{
        width: '100%',
        height: '100vh',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
      }}
    >
      <Card className={classes.root}>
        <CardContent>
          <Typography
            className={classes.title}
            color="textSecondary"
            gutterBottom
          >
            <TextField
              required
              id="standard-required"
              label="Title"
              value={title}
              onChange={(e) => setTitle(e.target.value)}
            />
          </Typography>
          <Typography
            className={classes.title}
            color="textSecondary"
            gutterBottom
          >
            <TextField
              required
              id="standard-requiredOne"
              label="Description"
              value={body}
              onChange={(e) => setBody(e.target.value)}
            />
          </Typography>
          <Typography
            className={classes.bullet}
            color="textSecondary"
            gutterBottom
          >
            <span>Upload image</span>
            <input type="file" onChange={(e) => setImg(e.target.files[0])} />
          </Typography>
        </CardContent>
        <CardActions style={{ display: 'flex', justifyContent: 'center' }}>
          <Button
            onClick={() => postDetail()}
            style={{ textAlign: 'center' }}
            size="large"
          >
            Submit post
          </Button>
        </CardActions>
      </Card>
    </div>
  );
}
