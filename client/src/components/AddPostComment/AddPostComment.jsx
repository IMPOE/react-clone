import React from 'react';
import { Field, Form, Formik } from 'formik';
import { object, string } from 'yup';
import {
  commentPostAction,
  getPosts,
  getSubPost,
} from '../../store/actions/postActions';
import { useDispatch, useSelector } from 'react-redux';
import './AddPostComment.scss';

const AddPostComment = ({ postId, userId }) => {
  const name = useSelector((state) => state.user.name);
  const pic = useSelector((state) => state.user.pic);
  const id = useSelector((state) => state.user.id);
  const dispatch = useDispatch();
  const following = useSelector((store) => store.user.following);
  const initialValues = {
    comment: '',
  };

  const validationSchema = object({
    comment: string().min(1).required(),
  });

  const commentSubmitHandler = async (
    { comment },
    { setSubmitting, resetForm }
  ) => {
    const onComplete = () => {
      resetForm({ values: '' });
      setSubmitting(false);
    };
    await dispatch(
      commentPostAction({ postId, text: comment, name, pic, id }, onComplete)
    );
    setTimeout(async () => {
      await dispatch(getPosts());
      await dispatch(getSubPost(following));
    }, 300);
  };

  return (
    <div className="add-post-comment">
      <Formik
        initialValues={initialValues}
        validationSchema={validationSchema}
        onSubmit={commentSubmitHandler}
      >
        {({ isSubmitting }) => (
          <Form>
            <Field
              name="comment"
              type="text"
              disabled={isSubmitting}
              placeholder="Add comment..."
            />
            <button type="submit" disabled={isSubmitting}>
              Send
            </button>
          </Form>
        )}
      </Formik>
    </div>
  );
};

export default AddPostComment;
