import { Avatar } from '@material-ui/core';
import { Link } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import '../../components/PostModal/components/PostModalComments/PostModalComments.scss';
import { ModalContent, ModalWindow } from '../../styled-components/modalStyles';
import Button from '../Button';
import { useEffect } from 'react';
import { getUserInfo } from '../../store/actions/userActions';

const InformativeModal = ({
  username,
  active,
  renderArr,
  hideModal,
  validationData,
  userName,
  chengeParam,
  setPostsData,
  setProfileData,
}) => {
  const dispatch = useDispatch();
  useEffect(async () => {
    await dispatch(getUserInfo(username, setProfileData, setPostsData));
  }, []);

  const userData = useSelector((state) => state.user);

  const renderUsers = () =>
    !renderArr ? (
      <h5>Loading....</h5>
    ) : renderArr.length > 0 ? (
      renderArr.map(({ pic, name, id }, index) => {
        return (
          <div
            style={{
              display: 'flex',
              flexDirection: 'column',
              justifyContent: 'center',
            }}
          >
            <li
              key={id}
              className="post-modal-comments__item"
              style={{
                display: 'flex',
                border: '1px solid gray',
                width: '80%',
                padding: '20px',
                margin: '10px auto',
              }}
            >
              <Avatar src={pic} style={{ width: 30, height: 30 }} />
              <Link
                className={'link'}
                onClick={() => hideModal()}
                to={userData.name !== name ? '/profile/' + name : '/profile'}
              >
                <span
                  onClick={chengeParam}
                  className="post-modal-comments__item__author"
                >
                  {name}
                </span>
              </Link>
            </li>
          </div>
        );
      })
    ) : (
      <h4 style={{ textAlign: 'center', margin: '40px 0 0 0' }}>
        {userName === userData?.name
          ? `You have no ${validationData} yet`
          : `${userName} have no ${validationData} yet`}
      </h4>
    );

  return (
    <ModalWindow
      className={active ? 'modal active' : 'modal '}
      onClick={hideModal}
    >
      <ModalContent
        className={active ? 'modal__content active' : 'modal__content'}
        style={{ backgroundColor: 'white', height: '50vh' }}
        onClick={(e) => {
          e.stopPropagation();
        }}
      >
        <div style={{ height: '20rem', overflowY: 'scroll' }}>
          {renderUsers()}
        </div>
        <div
          style={{
            margin: '0',
            display: 'flex',
            justifyContent: 'center',
            justifyItems: 'center',
          }}
        >
          <Button
            onButtonClick={hideModal}
            btnText={'Close'}
            style={{ margin: '50px 0 0 0', width: '200px' }}
            btnClassName={'btn'}
          />
        </div>
      </ModalContent>
    </ModalWindow>
  );
};

export default InformativeModal;
