import React from 'react';
import OptionsIcon from '../../../assests/OptionsIcon';
import './PostModalHeader.scss';
import { Avatar } from '@material-ui/core';

const PostModalHeader = ({ postedBy }) => {
  return (
    <div className="post-modal__header">
      <div className="post-modal__header__author">
        <div className="post-modal__header__author-img">
          <Avatar
            src={
              postedBy?.pic ||
              'https://www.pikpng.com/pngl/m/16-168770_user-iconset-no-profile-picture-icon-circle-clipart.png'
            }
            alt={postedBy?.name}
          />
        </div>
        <div>
          <p>{postedBy?.name}</p>
        </div>
      </div>
      <OptionsIcon />
    </div>
  );
};

export default PostModalHeader;
