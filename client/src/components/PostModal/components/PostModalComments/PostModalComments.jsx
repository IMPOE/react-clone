import React from 'react';
import './PostModalComments.scss';
import { Avatar } from '@material-ui/core';
import { useSelector } from 'react-redux';
import '../../../../App.css';
import { Link } from 'react-router-dom';

const PostModalComments = ({ comments }) => {
  const userOwnData = useSelector((state) => state.user);

  const renderComments = () =>
    comments &&
    comments.map(({ data, text }, index) => {
      return (
        <li
          key={index}
          className="post-modal-comments__item"
          style={{
            display: 'flex',
            border: '1px solid gray',
            width: '90%',
            padding: '5px',
          }}
        >
          <Avatar src={data?.pic} style={{ padding: '0 5px' }} />
          <Link
            className={'link'}
            to={
              userOwnData.name !== data?.name
                ? '/profile/' + data?.name
                : '/profile'
            }
          >
            <span
              className="post-modal-comments__item__author"
              style={{ padding: '0 5px' }}
            >
              {data?.name}:{' '}
            </span>
          </Link>
          <span
            className="post-modal-comments__item__comment"
            style={{ padding: '0 5px' }}
          >
            {text}{' '}
          </span>
        </li>
      );
    });

  return (
    <div className="post-modal-comments">
      <ul>{renderComments()}</ul>
    </div>
  );
};

export default PostModalComments;
