import React from 'react';
import { useSelector } from 'react-redux';
import userSelector from '../../store/selectors/userSelector';
import PostModalHeader from './components/PostModalHeader/PostModalHeader';
import PostModalComments from './components/PostModalComments/PostModalComments';
import AddPostComment from '../AddPostComment/AddPostComment';
import './PostModal.scss';

const PostModal = ({ post, onClose }) => {
  const { id: authorizedUserId } = useSelector(userSelector);
  const { _id: postId, comments, postedBy } = post;

  const handleModalClose = (event) => {
    event.target === event.currentTarget && onClose();
  };

  return (
    <div className="post-modal__wrapper" onClick={handleModalClose}>
      <div className="post-modal">
        <div className="post-modal__info">
          <PostModalHeader postedBy={postedBy} />
          <PostModalComments comments={comments} />
          <AddPostComment postId={postId} userId={postedBy?.id} />
        </div>
      </div>
    </div>
  );
};

export default PostModal;
