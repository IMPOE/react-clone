import React from 'react';
import './OnePost.scss';
const OnePost = (galleryPhoto) => {
  return (
    <figure className="container__image">
      <img src={galleryPhoto?.galleryPhoto} alt="" />
    </figure>
  );
};
export default OnePost;
