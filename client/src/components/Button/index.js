const Button = (props) => {

    const {btnText, btnClassName, onButtonClick, style, dataModal, id} = props;

    return(
        <button
            data-modal={dataModal}
            id={id}
            style={style}
            onClick={onButtonClick}
            className={btnClassName}
        >{btnText}</button>
    )
}

export default Button;