import styled from 'styled-components';

export const ModalHeader = styled.div`
  display: flex;
  justify-content: space-between;
  color: black;
  background-color: white;
  border: 1px solid #b6b6b6;
`;

export const ModalBody = styled.div`
  background-color: white;
  color: black;
`;

export const ModalWindow = styled.div`
  margin-top: 2rem;
  height: 100vh;
  width: 100vw;
  background: rgba(0, 0, 0, 0.6);
  position: fixed;
  top: 0;
  left: 0;
  display: flex;
  align-items: center;
  justify-content: center;
  opacity: 0;
  pointer-events: none;
  transition: 0.5s;
`;
export const ModalContent = styled.div`
  border-radius: 5px;
  width: 80%;
  transform: scale(0.3);
  transition: 0.4s all;
`;
export const CloseBtn = styled.button`
  position: relative;
  cursor: pointer;
  width: 30px;
  border: none;
`;
export const Close = styled.span`
  position: absolute;
  top: -5px;
  right: 10px;
  font-size: 40px;
  color: black;
`;
export const ModalFooter = styled.div`
  color: black;
  display: block;
`;
