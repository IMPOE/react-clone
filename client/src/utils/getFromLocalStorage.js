const getFromLocalStorage = (key, init) => {
  return localStorage.getItem(key) || init;
};

export default getFromLocalStorage;
