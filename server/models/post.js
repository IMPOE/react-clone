const mongoose = require('mongoose');
const { Schema } = require('mongoose');
const postSchema = new mongoose.Schema(
  {
    title: {
      type: String,
      required: true,
    },
    body: {
      type: String,
      required: true,
    },
    photo: {
      type: String,
      required: true,
    },
    postedBy: {
      type: Object,
      ref: 'User',
    },
    likes: [{ type: Object, ref: 'User' }],
    savedPost: [{ type: String, ref: 'User' }],
    comments: [
      {
        text: String,
        postedBy: { type: Schema.Types.ObjectId, ref: 'User' },
        data: Object,
      },
    ],
  },
  { timestamps: true }
);

mongoose.model('Post', postSchema);
