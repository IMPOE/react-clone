const mongoose = require('mongoose');
const mailService = require('./mailService');
const tokenService = require('./token-service');
const UserDto = require('../dtos/user-dtos');
const uuid = require('uuid');
const bcrypt = require('bcrypt');
const crypto = require('crypto');
const ApiError = require('../exceptions/api-error');
const User = mongoose.model('User');
const Post = mongoose.model('Post');
class UserService {

  async registration(email, password, name) {
    const userEmail = email.toLocaleLowerCase()
    console.log(userEmail)
    const candidate = await User.findOne({ "email" :  userEmail });
    if (candidate) {
      throw ApiError.BadRequest(
        `Пользователь с почтовым адресом ${email} уже существует`
      );
    }
    const hashPassword = await bcrypt.hash(password, 3);
    const activationLink = uuid.v4();

    const user = await User.create({
      email,
      password: hashPassword,
      activationLink,
      name,
    });
    await mailService.sendActivationMail(
      email,
      `${process.env.SERVER_URL}/activate/${activationLink}`
    );

    const userDto = new UserDto(user);
    const tokens = tokenService.generateTokens({ ...userDto });
    await tokenService.saveToken(userDto.id, tokens.refreshToken);

    return { ...tokens, user: userDto };
  }

  async activate(activationLink) {
    const user = await User.findOne({ activationLink });
    if (!user) {
      throw new Error('Неккоректная ссылка активации');
    }
    user.isActivated = true;
    await user.save();
  }

  async login(email, password) {
    const userEmail = email.toLocaleLowerCase()
    const user = await User.findOne({"email" : userEmail});
    if (!user) {
      throw ApiError.BadRequest('Пользователь с таким email не найден');
    }
    const isPassEquals = await bcrypt.compare(password, user.password);
    if (!isPassEquals) {
      throw ApiError.BadRequest('Неверный пароль');
    }
    const userDto = new UserDto(user);
    const tokens = tokenService.generateTokens({ ...userDto });

    await tokenService.saveToken(userDto.id, tokens.refreshToken);

    return { ...tokens, user: userDto };
  }

  async logout(refreshToken) {
    const token = await tokenService.removeToken(refreshToken);
    if (!refreshToken) {
      throw ApiError.UnauthorizedError();
    }
    return token;
  }

  async refresh(refreshToken) {
    if (!refreshToken) {
      throw ApiError.UnauthorizedError();
    }
    const userData = tokenService.validateRefreshToken(refreshToken);
    const tokenFromDb = await tokenService.findToken(refreshToken);
    if (!userData || !tokenFromDb) {
      throw ApiError.UnauthorizedError();
    }
    const user = await User.findById(userData.id);
    const userDto = new UserDto(user);
    const tokens = tokenService.generateTokens({ ...userDto });

    await tokenService.saveToken(userDto.id, tokens.refreshToken);
    return { ...tokens, user: userDto };
  }

  async getAllUsers() {
    const users = await User.find();
    if (!users) {
      throw new Error('Пользователей не найдено');
    }
    return users;
  }

  async resetPass(email) {
    const userEmail = email.toLocaleLowerCase()
    const user = await User.findOne({"email" : userEmail});
    if (!user) {
      throw new Error('Пользователь не найден');
    }
    crypto.randomBytes(32, (err, buffer) => {
      const token = buffer.toString('hex');
      if (err) {
        console.log(err);
      } else user.resetToken = token;
      user.expireToken = Date.now() + 3600000;
    });
    user.save();
    return user;
  }

  async changePass(sentToken, newPassword, email) {
    const userEmail = email.toLocaleLowerCase()
    const user = await User.findOne({
      resetToken: sentToken,
    });
    await mailService.sendPassword(userEmail, `${newPassword}`);
    const key = await bcrypt.hash(newPassword, 12);
    user.password = key;
    user.resetToken = undefined;
    user.expireToken = undefined;
    await user.save();
    return user;
  }

  async findMyPost(req) {
    const post = await Post.find({ 'postedBy.name': req.user.name }).populate(
      'PostedBy',
      'id name'
    );
    if (!post) {
      throw new Error('Постов не найдено');
    }
    return post;
  }

  async findAllPost() {
    const post = await Post.find();
    if (!post) {
      throw new Error('Посты не найдены');
    }
    return post;
  }

  async createPost(title, body, pic, req) {
    const post = await new Post({
      title,
      body,
      photo: pic,
      postedBy: req.user,
    });
    post.save();
    return post;
  }

  async like(req, res) {
    const liked = await Post.findOne({
      likes: req.user.id,
      _id: req.body.postId,
    });
    if (liked) {
      return false;
    } else
      await Post.findByIdAndUpdate(
        req.body.postId,
        {
          $push: {
            likes: { id: req.user.id, name: req.user.name, pic: req.body.pic },
          },
        },
        {
          new: true,
        }
      ).exec((err, result) => {
        if (err) {
          return res.status(422).json({ error: err });
        } else {
          res.json(result);
        }
      });
  }

  async unlike(req, res) {
    await Post.findByIdAndUpdate(
      req.body.postId,
      {
        $pull: {
          likes: { id: req.user.id, name: req.user.name, pic: req.body.pic },
        },
      },
      {
        new: true,
      }
    ).exec((err, result) => {
      if (err) {
        return res.status(422).json({ error: err });
      } else {
        res.json(result);
      }
    });
  }

  async comment(req, res) {
    const { data } = req.body;
    const comment = {
      text: data.text,
      postedBy: data.id,
      data: data,
    };
    Post.findByIdAndUpdate(
      data.postId,
      {
        $push: { comments: comment },
      },
      {
        new: true,
      }
    ).exec((err, result) => {
      if (err) {
        return res.status(422).json({ error: err });
      } else {
        res.json(result);
      }
    });
  }

  async getPassword(email) {
    const userEmail = email.toLocaleLowerCase()
    const user = await User.findOne({"email" : userEmail});
    if (!user) {
      throw ApiError.BadRequest('Пользователь с таким email не найден');
    }
    const password = uuid.v4();
    await mailService.forgotPassword(userEmail, password);
    const hashPassword = await bcrypt.hash(password, 3);
    user.password = hashPassword;
    const userDto = new UserDto(user);
    await user.save();
    return { user: userDto };
  }

  async savedPost(req, res) {
    Post.findByIdAndUpdate(
      req.body.userId,
      {
        $push: { savedPost: req.body.postId },
      },
      {
        new: true,
      }
    ).exec((err, result) => {
      if (err) {
        return res.status(422).json({ error: err });
      } else {
        res.json(result);
      }
    });
  }


  async savePhoto(pic, email) {
    const userEmail = email.toLocaleLowerCase()
    const user = await User.findOne({"email" : userEmail});
    if (!user) {
      throw new Error('Пользователь не найден');
    }
    user.pic = pic;
    await user.save();

    return user;
  }

  async findOnePost(postId) {
    const post = await Post.findById({ _id: postId });
    if (!post) {
      throw new Error('Пост не найден');
    }
    return post;
  }

  async searchUser(req, res) {
    let userPattern = new RegExp('^' + req.body.query);
    const userByName =  await User.find({
      name: { $regex: userPattern },
    })
      .select('_id email pic name')
      .then((user) => {
        if(user){
          return  res.json({ user });
        }

      })
      .catch((err) => {
        console.log(err);
      })
    const userByEmail =  User.find({
        email:{$regex:userPattern}})
        .select("_id email pic name")
        .then(user=>{
          if(user){
            res.json({user})
          }
        }).catch(err=>{
            console.log(err)
        })


  }

  async UnfollowUser(req, res) {
    User.findOneAndUpdate(
      req.body.unfollowName,
      {
        $pull: { followers: req.user.id },
      },
      { new: true },
      (err, result) => {
        if (err) {
          return res.status(422).json({ error: err });
        }
        User.findByIdAndUpdate(
          req.user.id,
          {
            $pull: { following: req.body.unfollowingId },
          },
          { new: true }
        )
          .then((result) => {
            res.json(result);
          })
          .catch((err) => {
            return res.status(422).json({ error: err });
          });
      }
    );
  }

  async getInfo(res, name) {
    const user = await User.findOne({ name: name });
    const post = await Post.find({ 'postedBy.name': name }).populate(
      'PostedBy',
      'id name'
    );
    if (!post) {
      throw new Error('Постов не найдено');
    }
    return res.json([user, post]);
  }

  async getData(name) {
    const user = await User.findOne({ name: name });
    if (!user) {
      throw new Error('Пользователь не найден');
    } else return user;
  }

  async follow(res, id, followingId) {
    User.findByIdAndUpdate(
      followingId,
      {
        $push: { followers: id },
      },
      { new: true },
      (err) => {
        if (err) {
          return res.status(422).json({ error: err });
        }
      }
    );
    User.findByIdAndUpdate(
      id,
      {
        $push: { following: followingId },
      },
      { new: true }
    )
      .then((result) => {
        res.json(result);
      })
      .catch((err) => {
        return res.status(422).json({ error: err });
      });
  }


  async changeAllPic (req,res,pic) {
      await Post.updateMany({ "postedBy.email" : req.user.email}, {
       "postedBy.pic" : pic
     })

  }

  async unFollow(res, id, unfollowingId) {
    await User.findByIdAndUpdate(
      unfollowingId,
      {
        $pull: { followers: id },
      },
      { new: true },
      (err, result) => {
        if (err) {
          return res.status(422).json({ error: err });
        }
      }
    );
    await User.findByIdAndUpdate(
      id,
      {
        $pull: { following: unfollowingId },
      },
      { new: true }
    )
      .then((result) => {
        res.json(result);
      })
      .catch((err) => {
        return res.status(422).json({ error: err });
      });
  }

  async getUserInform(res,name){
    const user = await User.findOne({ name: name });
    const post = await Post.find({ 'postedBy.name': name }).populate(
        'PostedBy',
        'id name'
    );
    if (!post) {
      throw new Error('Постов не найдено');
    }
    return res.json([user, post]);
  }


  async getUserDataInfo(name){
    const user = await User.findOne({ name: name });
    if (!user) {
      throw new Error('Пользователь не найден');
    }
      return user
  }
}

module.exports = new UserService();
