const nodemailer = require('nodemailer');
class MailService {
  constructor() {
    this.transporter = nodemailer.createTransport({
      host: process.env.SMTP_HOST,
      port: process.env.SMTP_PORT,
      secure: false,
      auth: {
        user: process.env.SMTP_USER,
        pass: process.env.SMTP_PASSWORD,
      },
    });
  }
  async sendActivationMail(to, link) {
    await this.transporter.sendMail({
      from: process.env.SMTP_USER,
      to,
      subject: 'Активация аккаунта на ' + process.env.SERVER_URL,
      text: 'Instagram clone',
      html: `<div>
                        <h2>Instagram clone </h2>
                        <h1>Для активации перейдите по ссылке</h1>
                        <a href="${link}">${link}</a>
                    </div>`,
    });
  }

  async sendPassword(to, password) {
    await this.transporter.sendMail({
      from: process.env.SMTP_USER,
      to,
      subject: 'Активация аккаунта на ' + process.env.SERVER_URL,
      text: `Instagram clone`,
      html: `
                    <div>
                        <h1>Your new password</h1>
                        <h2>${password}</h2>
                    </div> `,
    });
  }

  async forgotPassword(to, password) {
    await this.transporter.sendMail({
      from: process.env.SMTP_USER,
      to,
      subject: 'Активация аккаунта на ' + process.env.SERVER_URL,
      text: `Instagram clone`,
      html: `
                    <div>
                        <h1>Your new password</h1>
                        <h2>${password}</h2>
                    </div> `,
    });
  }
}

module.exports = new MailService();
