module.exports = class UserDto {
  email;
  id;
  isActivated;
  name;
  resetToken;
  expireToken;
  followers;
  following;
  pic;
  notification;
  constructor(model) {
    this.email = model.email;
    this.id = model._id;
    this.isActivated = model.isActivated;
    this.name = model.name;
    this.resetToken = model.resetToken;
    this.expireToken = model.expireToken;
    this.followers = model.followers;
    this.following = model.following;
    this.pic = model.pic;
    this.notification = model.notification;
  }
};
