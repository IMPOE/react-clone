const express = require('express');
const router = express.Router();
const userController = require('../controllers/user-controller');
const { body } = require('express-validator');
const authMiddleware = require('../middleware/auth-middleware');

router.get('/activate/:link', userController.activate);
router.get('/refresh', authMiddleware, userController.refresh);
router.get('/users', userController.getUsers);
router.post('/forgotpassword', userController.getPassword);
router.post(
  '/signup',
  body('email').isEmail(),
  body('password').isLength({ min: 3, max: 32 }),
  body('name').isLength({ min: 3, max: 32 }),
  userController.registration
);
router.post('/signin', userController.login);
router.post('/logout', userController.logout);
router.post('/reset-password', authMiddleware, userController.resetPassword);
router.post(
  '/new-password',
  authMiddleware,
  body('password').isLength({ min: 3, max: 32 }),
  userController.changePassword
);

module.exports = router;
