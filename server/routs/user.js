const express = require('express');
const router = express.Router();
const userController = require('../controllers/user-controller');
const authMiddleware = require('../middleware/auth-middleware');
const mongoose = require('mongoose');
const User = mongoose.model('User');

router.get('/user/:name',authMiddleware, userController.getUserInfo)


router.get('/userdata', authMiddleware, async (req, res) => {
;
});
router.put('/follow', authMiddleware, userController.followUser);
router.put('/unfollow', authMiddleware, userController.unFollowUser);
router.post('/changephoto', authMiddleware, userController.changePhoto);
router.post('/deletphoto', authMiddleware, userController.deletePhoto);
router.post('/search-users', authMiddleware, userController.search);
module.exports = router;
