const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const userController = require('../controllers/user-controller');
const authMiddleware = require('../middleware/auth-middleware');
const Post = mongoose.model('Post');

router.get('/allpost', userController.getAllPost);
router.post('/onepost', userController.getOnePost);
router.post('/createpost', authMiddleware, userController.createPost);
router.get('/mypost', authMiddleware, userController.getMyPost);
router.put('/like', authMiddleware, userController.like);
router.put('/unlike', authMiddleware, userController.unlike);
router.put('/comment', authMiddleware, userController.comment);
router.put('/savedpost', authMiddleware, userController.savePost);

router.delete('/deletepost', authMiddleware, async (req, res) => {
  const post = await Post.findById({ _id: req.body.postId }).exec(
    (err, post) => {
      if (err || !post) {
        return res.status(422).json({ err });
      }
      if (post.postedBy.id === req.user.id) {
        post
          .remove()
          .then((result) => {
            res.json({ post });
          })
          .catch((err) => {
            return res
              .status(422)
              .json({ err: 'Вы не являетесь владельцем данного поста!' });
          });
      } else {
        return res.json({ err: 'Вы не являетесь владельцем данного поста!' });
      }
    }
  );
});

router.put('/getsubpost', async (req, res) => {
  const { following } = req.body;
  await Post.find({ 'postedBy.id': { $in: following } })
    .sort('-createdAt')
    .then((posts) => {
      res.json({ posts });
    })
    .catch((err) => {
      console.log(err);
    });
});

router.put('/getsavedpost', async (req, res) => {
  const { userId } = req.body;
  await Post.find({ savedPost: userId })
    .sort('-createdAt')
    .then((posts) => {
      res.json({ posts });
    })
    .catch((err) => {
      console.log(err);
    });
});

module.exports = router;
