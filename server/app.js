require('dotenv').config()
const express = require("express")
const mongoose =require("mongoose")
const errorMiddleware = require('./middleware/error-middleware');
const cookieParser = require('cookie-parser')
const cors = require('cors')
const bodyParser = require('body-parser');
const app = express()
mongoose.set('useFindAndModify', false);

const PORT = process.env.PORT || 5000;
require("./models/user")
require("./models/post")
require("./models/token")

app.use(express.json())
app.use(cors())
app.use(cookieParser())
app.use(express.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json())


app.use(require("./routs/auth"))
app.use(require("./routs/post"))
app.use(require("./routs/user"))
app.use(errorMiddleware);


// app.use(express.static(__dirname + "/build"))
//
// app.get("*",(req,res) => {
//     res.sendFile(__dirname + "/build/index.html")
// })
const start = async () => {
    try {
        await mongoose.connect(process.env.DB_URL, {
            useNewUrlParser: true,
            useUnifiedTopology: true
        })
        app.listen(PORT, () => console.log(`Server started on PORT = ${PORT}`))
    } catch (e) {
        console.log(e);
    }
}

start()
