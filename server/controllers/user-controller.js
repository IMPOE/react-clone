const userService = require('../service/userService');
const { validationResult } = require('express-validator');
const ApiError = require('../exceptions/api-error');

class UserController {
  async registration(req, res, next) {
    try {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return next(ApiError.NotFound('Ошибка при валидации', errors.array()));
      }
      const { email, password, name } = req.body;
      const userData = await userService.registration(email, password, name);
      res.cookie('refreshToken', userData.refreshToken, {
        maxAge: 30 * 24 * 60 * 60 * 1000,
        httpOnly: true,
      });
      return res.json(userData);
    } catch (e) {
      next(e);
    }
  }

  async login(req, res, next) {
    try {
      const { email, password, name } = req.body;
      const userData = await userService.login(email, password, name);
      res.cookie('refreshToken', userData.refreshToken, {
        maxAge: 30 * 24 * 60 * 60 * 1000,
        httpOnly: true,
      });
      return res.json(userData);
    } catch (e) {
      next(e);
    }
  }

  async activate(req, res, next) {
    try {
      const activationLink = req.params.link;
      await userService.activate(activationLink);
      return res.redirect(process.env.CLIENT_URL_LOGIN);
    } catch (e) {
      next(e);
    }
  }
  async logout(req, res, next) {
    try {
      const { refreshToken } = req.cookies;
      await userService.logout(refreshToken);
      res.clearCookie('refreshToken');
      return res.json(refreshToken);
    } catch (e) {
      next(e);
    }
  }
  async refresh(req, res, next) {
    try {
      const { refreshToken } = req.cookies;
      const userData = await userService.refresh(refreshToken);
      res.cookie('refreshToken', userData.refreshToken, {
        maxAge: 30 * 24 * 60 * 60 * 1000,
        httpOnly: true,
      });
      return res.json(userData);
    } catch (e) {
      next(e);
    }
  }

  async createPost(req, res, next) {
    try {
      const { title, body, pic } = req.body;
      if (!title || !body || !pic) {
        return res.status(422).json({ err: 'please add all the fields' });
      }
      req.user.password = undefined;
      await userService
        .createPost(title, body, pic, req)
        .then((result) => {
          res.json({ post: result });
        })
        .catch((err) => {
          console.log(err);
        });
    } catch (e) {
      next(e);
    }
  }

  async getUsers(req, res, next) {
    try {
      const users = await userService.getAllUsers();
      return res.json(users);
    } catch (e) {
      next(e);
    }
  }
  async resetPassword(req, res, next) {
    try {
      const { email } = req.user;
      const user = await userService.resetPass(email);
      return res.json(user);
    } catch (e) {
      next(e);
    }
  }
  async changePassword(req, res, next) {
    try {
      const newPassword = req.body.password;
      const sentToken = req.body.resetToken;
      const email = req.user.email;
      const user = await userService.changePass(sentToken, newPassword, email);
      return res.json(user);
    } catch (e) {
      next(e);
    }
  }
  async getMyPost(req, res, next) {
    try {
      const post = await userService.findMyPost(req);
      return res.json({ post: post });
    } catch (e) {
      next(e);
    }
  }
  async getAllPost(req, res, next) {
    try {
      const post = await userService.findAllPost();
      return res.json(post);
    } catch (e) {
      next(e);
    }
  }

  async like(req, res, next) {
    try {
      await userService.like(req, res);
    } catch (e) {
      next(e);
    }
  }

  async unlike(req, res, next) {
    try {
      await userService.unlike(req, res);
    } catch (e) {
      next(e);
    }
  }

  async comment(req, res, next) {
    try {
      await userService.comment(req, res);
    } catch (e) {
      next(e);
    }
  }

  async getPassword(req, res, next) {
    try {
      const email = req.body.email;
      if (!email) {
        throw ApiError.BadRequest(`User not found `);
      }
      const user = await userService.getPassword(email);
      return res.json(user);
    } catch (e) {
      next(e);
    }
  }

  async savePost(req, res, next) {
    try {
      await userService.savedPost(req, res);
    } catch (e) {
      next(e);
    }
  }

  async changePhoto(req, res, next) {
    try {
      const { pic } = req.body;
      const { email } = req.user;
      console.log(pic)
      if (!pic) {
        return res.status(422).json({ err: 'please add all the fields' });
      }
      req.user.password = undefined;
      await userService
        .savePhoto(pic, email)
        .then(async (result) => {
          res.json({user: result});
          await userService.changeAllPic(req, res, pic);
        })
        .catch((err) => {
          console.log(err);
        });
    } catch (e) {
      next(e);
    }
  }

  async deletePhoto(req, res, next) {
    try {
      const { email } = req.user;
      if (!email) {
        return res.status(422).json({ err: 'user not found!' });
      }
      req.user.password = undefined;
      await userService
        .changePhoto(email)
        .then((result) => {
          res.json({ user: result });
        })
        .catch((err) => {
          console.log(err);
        });
    } catch (e) {
      next(e);
    }
  }

  async getOnePost(req, res, next) {
    try {
      const postId = req.body.postId;
      if (!postId) {
        return res.status(422).json({ err: 'post not found!' });
      }
      const userPost = await userService.findOnePost(postId);
      res.json({ userPost });
    } catch (e) {
      next(e);
    }
  }

  async search(req, res, next) {
    try {
      await userService.searchUser(req, res);
    } catch (e) {
      next(e);
    }
  }

  async unfollow(req, res, next) {
    try {
      await userService.UnfollowUser(req, res);
    } catch (e) {
      next(e);
    }
  }

  async getUserInfo(req, res, next) {
    try {
      const { name } = req.params;
      await userService.getInfo(res, name);
    } catch (e) {
      next(e);
    }
  }

  async getUserData(req, res, next) {
    try {
      const { name } = req.user;
      const user = await userService.getData(name);
      return res.json(user);
    } catch (e) {
      next(e);
    }
  }

  async followUser(req, res, next) {
    try {
      const { id } = req.user;
      const { followingId } = req.body;
      await userService.follow(res, id, followingId);
    } catch (e) {
      next(e);
    }
  }

  async unFollowUser(req, res, next) {
    try {
      const { id } = req.user;
      const { unfollowingId } = req.body;
      await userService.unFollow(res, id, unfollowingId);
    } catch (e) {
      next(e);
    }
  }

  async getUserInfo(req, res, next){
    try {
    const {name} = req.params
      await userService.getUserInform(res,name);
    } catch (e) {
      next(e);
    }
  }

  async getUserData(req, res, next){
    try {
      const {name} = req.user
     const user =  await userService.getUserDataInfo(name);
      return res.json(user)
    } catch (e) {
      next(e);
    }
  }
}

module.exports = new UserController();
